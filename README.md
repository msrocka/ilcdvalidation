# ILCD Validation API

## Building from source
In order to build this project from source you need to have the following tools
installed:

* A [Java Development Kit >= 7](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
* [Apache Maven](https://maven.apache.org/)
* [Apache Ant](http://ant.apache.org/) 

It is a standard Maven project but Ant is used to package the validation
profiles in the project. When you have these tools installed you should be able
to build project with the following steps:

Get the source via Git (or simply download it) and switch to the project folder:

```bash
git clone https://bitbucket.org/okusche/ilcdvalidation.git
cd ilcdvalidation
```

Package the validation profiles with Ant:

```bash
ant
```

Build the API and install it into your local Maven repository:

```bash
mvn install
```

## License

Copyright 2015 Oliver Kusche

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.