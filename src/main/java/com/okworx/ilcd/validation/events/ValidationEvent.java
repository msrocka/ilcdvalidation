package com.okworx.ilcd.validation.events;

import com.okworx.ilcd.validation.reference.IDatasetReference;

public class ValidationEvent extends AbstractValidationEvent implements IValidationEvent {

	public static final String SUCCESS_MESSAGE = "The dataset has been successfully validated.";
	
	public ValidationEvent(String aspect, Severity severity, Type type, IDatasetReference reference, String message) {
		super(aspect, severity, type, reference, message);
	}

	public ValidationEvent(String aspect, Severity severity, IDatasetReference reference, String message) {
		super(aspect, severity, reference, message);
	}

	public ValidationEvent(String aspect) {
		super(aspect);
	}

	public ValidationEvent() {
		super();
	}

}
