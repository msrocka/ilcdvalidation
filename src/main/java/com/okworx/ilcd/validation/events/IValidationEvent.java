package com.okworx.ilcd.validation.events;

import com.okworx.ilcd.validation.reference.IDatasetReference;

public interface IValidationEvent {

	public Type getType();

	public Severity getSeverity();

	public String getMessage();

	public String getAspect();

	public IDatasetReference getReference();

	public void setMessage(String message);

	public void setType(Type type);

	public void setReference(IDatasetReference reference);

	public void setSeverity(Severity severity);

	public String toString();
}
