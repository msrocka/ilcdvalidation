package com.okworx.ilcd.validation.events;

import org.apache.commons.lang3.StringUtils;

import com.okworx.ilcd.validation.reference.IDatasetReference;

/**
 * @author oli
 *
 */
public abstract class AbstractValidationEvent implements IValidationEvent {

	protected String aspect;

	protected String message;

	protected IDatasetReference reference;

	protected Type type = Type.GENERIC;

	protected Severity severity;

	public AbstractValidationEvent(String aspect, Severity severity, Type type, IDatasetReference reference, String message) {
		this.aspect = aspect;
		this.severity = severity;
		this.type = type;
		this.reference = reference;
		this.message = message;
	}
	
	public AbstractValidationEvent(String aspect, Severity severity, IDatasetReference reference, String message) {
		this.aspect = aspect;
		this.severity = severity;
		this.type = Type.GENERIC;
		this.reference = reference;
		this.message = message;
	}
	
	public AbstractValidationEvent(String aspect) {
		this.aspect = aspect;
	}

	public AbstractValidationEvent() {
	}

	public String getMessage() {
		return message;
	}

	public IDatasetReference getReference() {
		return reference;
	}

	public Type getType() {
		return type;
	}

	public Severity getSeverity() {
		return severity;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setReference(IDatasetReference reference) {
		this.reference = reference;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public void setSeverity(Severity severity) {
		this.severity = severity;
	}

	public String getAspect() {
		return aspect;
	}

	public void setAspect(String aspect) {
		this.aspect = aspect;
	}

	public String toString() {
		StringBuffer buf = new StringBuffer(this.getAspect());
		buf.append(" ");
		buf.append(this.severity.getValue());
		buf.append(" ");
		buf.append(this.type.getValue());
		buf.append(" ");
		buf.append(StringUtils.isNotBlank(this.getReference().getUuid()) ? this.getReference().getUuid() + " ": "");
		buf.append(StringUtils.isNotBlank(this.getReference().getName()) ? this.getReference().getName() + " ": "");
		buf.append(this.getReference().getShortFileName());
		buf.append(" ");
		buf.append(this.getReference().getType()!=null ? this.getReference().getType() : "");
		buf.append(" ");
		buf.append(this.message);
		return buf.toString();
	}
}
