package com.okworx.ilcd.validation.events;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Holds a list of validation events for a certain validation aspect.
 * 
 * When chaining Validators, the nestedEvents property contains the separate
 * EventsList objects in order to keep track of the respective validation
 * results
 * 
 * @author oliver.kusche
 * 
 */
public class EventsList {

	protected String aspectName;

	protected boolean hasErrors = false;

	protected boolean hasWarnings = false;

	protected int errorCount = 0;

	protected int warningCount = 0;

	protected int successCount = 0;

	protected List<IValidationEvent> events = new ArrayList<IValidationEvent>();

	public EventsList(String aspectName) {
		this.aspectName = aspectName;
	}

	public void add(IValidationEvent event) {
		if (event.getSeverity().equals(Severity.ERROR)) {
			this.hasErrors = true;
			this.errorCount++;
		}
		if (event.getSeverity().equals(Severity.WARNING)) {
			this.hasWarnings = true;
			this.warningCount++;
		}
		if (event.getSeverity().equals(Severity.SUCCESS)) {
			this.successCount++;
		}
		this.events.add(event);
	}

	public void addAll(Collection<IValidationEvent> events) {
		for (IValidationEvent event : events)
			this.add(event);
	}

	public boolean isEmpty() {
		return this.events.isEmpty();
	}

	/**
	 * Indicates whether this list does not contain any errors or warnings.
	 * 
	 * @return
	 */
	public boolean isPositive() {
		return !(this.hasErrors || this.hasWarnings);
	}

	/**
	 * Indicates whether this list contains any errors.
	 * 
	 * @return
	 */
	public boolean hasErrors() {
		return this.hasErrors;
	}

	/**
	 * Indicates whether this list contains any warnings.
	 * 
	 * @return
	 */
	public boolean hasWarnings() {
		return this.hasWarnings;
	}

	public String getAspectName() {
		return aspectName;
	}

	public void setAspectName(String aspectName) {
		this.aspectName = aspectName;
	}

	public List<IValidationEvent> getEvents() {
		return events;
	}

	public void setEvents(List<IValidationEvent> events) {
		this.events = events;
	}

	public int size() {
		return this.events.size();
	}

	public String[] toArray() {
		return (String[]) this.events.toArray();
	}

	public Integer getErrorCount() {
		return errorCount;
	}
	
	public Integer getWarningCount() {
		return warningCount;
	}

	public Integer getSuccessCount() {
		return successCount;
	}

}
