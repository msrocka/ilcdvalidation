package com.okworx.ilcd.validation;

import java.io.File;
import java.util.HashMap;

import com.okworx.ilcd.validation.profile.Profile;
import com.okworx.ilcd.validation.reference.IDatasetReference;
import com.okworx.ilcd.validation.util.IUpdateEventListener;
import com.okworx.ilcd.validation.util.Statistics;

public interface IDatasetsValidator extends IValidator {

	public abstract void setObjectsToValidate(HashMap<String, IDatasetReference> objects);

	public abstract void setObjectsToValidate(File directory);

	public abstract HashMap<String, IDatasetReference> getObjectsToValidate();

	public abstract void setProfile(Profile profile);

	public abstract Profile getProfile();

	public abstract boolean validate() throws InterruptedException;

	public IUpdateEventListener getUpdateEventListener();

	public void setUpdateEventListener(IUpdateEventListener listener);

	public abstract void setUpdateInterval(int updateInterval);

	public abstract void setValidateArchives(boolean validateArchives);
	
	public Statistics getStatistics();


}