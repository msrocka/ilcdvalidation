package com.okworx.ilcd.validation;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.zip.GZIPInputStream;

import com.okworx.ilcd.validation.profile.Profile;
import com.okworx.ilcd.validation.util.PrefixBuilder;

public abstract class AbstractReferenceObjectsAwareValidator extends AbstractDatasetsValidator {

	protected HashMap<String, String> referenceObjects;

	@Override
	public void setProfile(Profile profile) {
		super.setProfile(profile);
		if ( profile != null )
			setupReferenceObjects(profile.getReferenceElementaryFlows());
	}

	public void setReferenceObjects(HashMap<String, String> map) {
		this.referenceObjects = map;
	}

	public HashMap<String, String> getReferenceObjects() {
		return this.referenceObjects;
	}

	@SuppressWarnings("unchecked")
	private void setupReferenceObjects(String file) {
	
		HashMap<String, String> map = new HashMap<String, String>();
	
		try {
			URL url = new URL(PrefixBuilder.buildPath(profile.getPath().toString(), file));
	
			InputStream is = url.openStream();
			GZIPInputStream gz = new GZIPInputStream(is);
			ObjectInputStream ois = new ObjectInputStream(gz);
			map = (HashMap<String, String>) ois.readObject();
			ois.close();
		} catch (FileNotFoundException e) {
			log.error(e);
		} catch (ClassNotFoundException e) {
			log.error(e);
		} catch (IOException e) {
			log.error(e);
		}
	
		this.setReferenceObjects(map);
	
		log.debug("reference objects map has " + map.size() + " items");
	
	}

}
