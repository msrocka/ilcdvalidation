package com.okworx.ilcd.validation;

import com.okworx.ilcd.validation.events.EventsList;

public interface IValidator {

	/**
	 * @return the name of the validation aspect covered by this Validator.
	 */
	public abstract String getAspectName();

	/**
	 * @return a description of the validation aspect covered by this Validator.
	 */
	public abstract String getAspectDescription();

	/**
	 * Invokes the validation
	 * 
	 * @return the validation result
	 */
	public boolean validate() throws InterruptedException;

	/**
	 * @return the list of validation events
	 */
	public EventsList getEventsList();

	/**
	 * @param name
	 * @param obj
	 */
	public void setParameter( String name, Object obj );

	/**
	 * Whether to report successful validations (default: false)
	 * 
	 * @param reportSuccesses
	 */
	public void setReportSuccesses(boolean reportSuccesses);

}
