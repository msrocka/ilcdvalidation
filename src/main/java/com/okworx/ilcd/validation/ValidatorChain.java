package com.okworx.ilcd.validation;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.okworx.ilcd.validation.profile.ProfileManager;
import com.okworx.ilcd.validation.util.IUpdateEventListener;

/**
 * Allows to chain multiple Validators that all work on the same set of Objects
 * 
 * @author oliver.kusche
 *
 */
public class ValidatorChain extends AbstractDatasetsValidator implements IUpdateEventListener {

	protected final Logger log = org.apache.log4j.Logger.getLogger(this.getClass());

	protected List<IDatasetsValidator> validators = new ArrayList<IDatasetsValidator>();

	private String aspectName;

	private String aspectDescription;
	
	private int finishedValidators = 0;

	public ValidatorChain(String aspectName) {
		super();
		this.aspectName = aspectName;
		this.eventsList.setAspectName(aspectName);
	}

	public ValidatorChain() {
		super();
		this.setProfile(ProfileManager.INSTANCE.getDefaultProfile());
	}
	
	public boolean validate() {

		boolean pass = this.eventsList.isPositive();

		boolean validateArchiveFlag = true;
		
		this.statistics = null;
		
		for (IDatasetsValidator v : this.validators) {
			if (validateArchiveFlag) {
				v.setValidateArchives(false);
				validateArchiveFlag=false;
			}
			v.setUpdateEventListener(this);
			v.setUpdateInterval(this.getUpdateInterval());
			v.setReportSuccesses(this.reportSuccesses);
			v.setProfile(this.profile);
		}

		try {
			for (IDatasetsValidator v : this.validators) {
				if (Thread.currentThread().isInterrupted()) {
					log.info("operation was interrupted, aborting");
					updateStatusCancelled();
					break;
				}

				v.setObjectsToValidate(this.objectsToValidate);
				
				if (v.validate() == false || !v.getEventsList().isPositive())
					pass = false;
				
				this.eventsList.addAll(v.getEventsList().getEvents());
				
				if (this.statistics == null)
					this.statistics = v.getStatistics();
				else
					this.statistics.merge(v.getStatistics());
				
				finishedValidators++;
			}
			
		} catch (InterruptedException e) {
			updateStatusCancelled();
			updateProgress(0);
			return false;
		}

		return pass;
	}

	public void add(IDatasetsValidator validator) {
		this.validators.add(validator);
	}

	public List<IDatasetsValidator> getValidators() {
		return validators;
	}

	public void setValidators(List<IDatasetsValidator> validators) {
		this.validators = validators;
	}

	public String getAspectName() {
		return aspectName;
	}

	public void setAspectName(String aspectName) {
		this.aspectName = aspectName;
		this.eventsList.setAspectName(aspectName);
	}

	public String getAspectDescription() {
		return aspectDescription;
	}

	public void setAspectDescription(String aspectDescription) {
		this.aspectDescription = aspectDescription;
	}

	public void updateProgress(double percentFinished) {
		if (this.getUpdateEventListener() != null)
			this.getUpdateEventListener().updateProgress((this.finishedValidators + percentFinished ) / this.validators.size());
	}

	public void updateStatus(String statusMessage) {
		if (this.getUpdateEventListener() != null)
			this.getUpdateEventListener().updateStatus(statusMessage);		
	}

}
