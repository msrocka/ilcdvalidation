package com.okworx.ilcd.validation;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import net.java.truevfs.access.TFile;
import net.java.truevfs.access.TFileInputStream;

import org.apache.commons.io.output.NullOutputStream;

import com.okworx.ilcd.validation.common.DatasetType;
import com.okworx.ilcd.validation.events.IValidationEvent;
import com.okworx.ilcd.validation.events.Severity;
import com.okworx.ilcd.validation.events.ValidationEvent;
import com.okworx.ilcd.validation.profile.Profile;
import com.okworx.ilcd.validation.profile.ProfileManager;
import com.okworx.ilcd.validation.reference.IDatasetReference;
import com.okworx.ilcd.validation.util.AbstractDatasetsTask;
import com.okworx.ilcd.validation.util.CURIResolver;
import com.okworx.ilcd.validation.util.PartitionedList;
import com.okworx.ilcd.validation.util.PrefixBuilder;
import com.okworx.ilcd.validation.util.TaskResult;
import com.okworx.ilcd.validation.util.ValidatorListener;

/**
 * Validates a set of given datasets against an XSLT Stylesheet.
 * 
 * @author oliver.kusche
 *
 */
public class XSLTStylesheetValidator extends AbstractDatasetsValidator implements IValidator {

	private String aspectName = "Custom Validity";

	private String aspectDescription = "Checks against a custom XSLT stylesheet.";

	protected String urlPrefix;
	protected String stylesheetName;

	protected Map<String, Object> transformationParameters = new HashMap<String, Object>();

	protected Map<String, String> resolverMappings = new HashMap<String, String>();

	public void setTransformationParameter(String string, Object object) {
		this.transformationParameters.put(string, object);
	}

	public XSLTStylesheetValidator() {
		super();
		this.setProfile(ProfileManager.INSTANCE.getDefaultProfile());
	}

	@Override
	public String getAspectName() {
		return this.aspectName;
	}

	@Override
	public String getAspectDescription() {
		return this.aspectDescription;
	}

	@Override
	public void setProfile(Profile profile) {
		super.setProfile(profile);
		if ( profile != null ) {
			registerStylesheet(profile.getPath().toString(), profile.getStylesheetsPath(),
				profile.getValidationStylesheet());
		}
	}

	public void registerStylesheet(String pathToJar, String stylesheetsDir, String stylesheetName) {
		this.urlPrefix = PrefixBuilder.buildPrefix(pathToJar, stylesheetsDir);
		this.stylesheetName = stylesheetName;
	}

	public boolean validate() throws InterruptedException {

		updateStatusSetup();

		super.validate();

		if (this.stylesheetName == null)
			throw new IllegalArgumentException("no stylesheet set");
		if (this.urlPrefix == null)
			throw new IllegalArgumentException("no schemas location registered");

		this.unitsTotal = this.objectsToValidate.size();

		PartitionedList<IDatasetReference> partList = new PartitionedList<IDatasetReference>(
				this.objectsToValidate.values());

		Collection<Callable<TaskResult>> tasks = new ArrayList<Callable<TaskResult>>();

		for (List<IDatasetReference> refList : partList.getPartitions()) {
			try {
				tasks.add(new ValidateTask(this, refList, this.urlPrefix, this.resolverMappings));
			} catch (TransformerConfigurationException e) {
				log.error("error configuring transformer", e);
			} catch (TransformerFactoryConfigurationError e) {
				log.error("error configuring transformer factory", e);
			}
		}

		ExecutorService executor = Executors.newFixedThreadPool(partList.getNumThreads());

		updateStatusValidating();

		try {
			List<Future<TaskResult>> taskResults = executor.invokeAll(tasks);
			for (Future<TaskResult> taskResult : taskResults) {
				if (taskResult.get() != null) {
					TaskResult res = taskResult.get();
					this.eventsList.addAll(res.getValidationEvents());
					this.statistics.add(res.getStatistics());
				}
			}
			executor.shutdown();
		} catch (InterruptedException e) {
			executor.shutdown();
			interrupted(e);
		} catch (Exception e) {
			log.error(e);
		}

		log.info(this.eventsList.getEvents().size() + " events ");

		updateProgress(1);
		updateStatusDone();

		return this.getEventsList().isPositive();

	}

	private javax.xml.transform.Transformer setupTransformer(ValidatorListener listener)
			throws TransformerFactoryConfigurationError, TransformerConfigurationException {
		TransformerFactory tFactory = TransformerFactory.newInstance();

//		CURIResolver resolver = new CURIResolver(this.urlPrefix);

//		for (String key : this.resolverMappings.keySet())
//			resolver.registerMapping(key, this.resolverMappings.get(key));

//		tFactory.setURIResolver(resolver);

		javax.xml.transform.Transformer transformer = tFactory.newTransformer(new StreamSource(this.urlPrefix
				+ this.stylesheetName));

		transformer.setErrorListener(listener);

		for (String key : this.transformationParameters.keySet()) {
			if (log.isDebugEnabled())
				log.debug("setting transformer variable '" + key + "' to " + this.transformationParameters.get(key));
			transformer.setParameter(key, this.transformationParameters.get(key));
		}

		return transformer;
	}

	public void setAspectName(String aspectName) {
		this.aspectName = aspectName;
		this.eventsList.setAspectName(aspectName);
	}
	
	final class ValidateTask extends AbstractDatasetsTask implements Callable<TaskResult> {

		private String urlPrefix;
		private ValidatorListener vListener;
		private Transformer transformer;
		private Map<String, String> resolverMappings;

		ValidateTask(AbstractDatasetsValidator validator, Collection<IDatasetReference> files, String urlPrefix, Map<String, String> resolverMappings) throws TransformerConfigurationException, TransformerFactoryConfigurationError {
			this.files = files;
			this.validator = validator;
			this.urlPrefix = urlPrefix;
			this.resolverMappings = resolverMappings;
			this.vListener = new ValidatorListener();
			this.transformer = setupTransformer(this.vListener);
		}

		public TaskResult call() throws Exception {
			return new TaskResult(validate(files), this.statistics);
		}

		private Collection<IValidationEvent> validate(Collection<IDatasetReference> files) throws Exception {

			TFileInputStream tis = null;

			Collection<IValidationEvent> events = new ArrayList<IValidationEvent>();
			
			int count = 0;

			for (IDatasetReference ref : files) {
				if (Thread.currentThread().isInterrupted()) {
					log.info("operation was interrupted, aborting");
					updateStatusCancelled();
					throw new InterruptedException();
				}

				if (ref.getType().equals(DatasetType.EXTERNAL_FILE)) {
					this.statistics.update(ref, true);
					continue;
				}

				tis = new TFileInputStream(ref.getAbsoluteFileName());
				try {
					String path = new TFile(ref.getAbsoluteFileName()).getParent().concat(File.separator);
					transformer.setURIResolver(new CURIResolver(this.urlPrefix, path, this.resolverMappings));
					transformer.setParameter("pathPrefix", path);
					transformer.transform(new StreamSource(tis), new StreamResult(new NullOutputStream()));
				} catch (Exception e) {
					String message = e.getMessage().replaceAll("com.sun.org.apache.xalan.internal.xsltc.TransletException:", "").replaceAll("java.io.FileNotFoundException", "File not found");
					Severity severity = (message.trim().startsWith("File not found") ? Severity.WARNING : Severity.ERROR);				
					events.add(new ValidationEvent(XSLTStylesheetValidator.this.getAspectName(), severity, ref, message));			
				}
				
				boolean success = (vListener.getResults().isEmpty());
				this.statistics.update(ref, success);

				if (success && XSLTStylesheetValidator.this.reportSuccesses)
					events.add(new ValidationEvent(XSLTStylesheetValidator.this.getAspectName(), Severity.SUCCESS, ref, ValidationEvent.SUCCESS_MESSAGE));
				
				count = updateChunkCount(count);

				if (log.isDebugEnabled())
					log.debug(ref.getUuid() + ": " + vListener.getResults().size() + " events occurred");
				// extract message
				for (String rawMessage : vListener.getResults()) {
					if (rawMessage.startsWith("Validation error: ")) {
						rawMessage = rawMessage.split("Validation error: ")[1];
						events.add(new ValidationEvent(XSLTStylesheetValidator.this.getAspectName(), Severity.ERROR, ref, rawMessage));
					} 
					else if (rawMessage.startsWith("Validation warning: ")) {
						rawMessage = rawMessage.split("Validation warning: ")[1];
						events.add(new ValidationEvent(XSLTStylesheetValidator.this.getAspectName(), Severity.WARNING, ref, rawMessage));
					}
				}
				vListener.getResults().clear();
			}
			return events;
			
		}
	}

}
