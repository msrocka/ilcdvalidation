package com.okworx.ilcd.validation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import net.java.truevfs.access.TFileInputStream;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.okworx.ilcd.validation.common.DatasetType;
import com.okworx.ilcd.validation.events.IValidationEvent;
import com.okworx.ilcd.validation.events.Severity;
import com.okworx.ilcd.validation.events.ValidationEvent;
import com.okworx.ilcd.validation.reference.DatasetReference;
import com.okworx.ilcd.validation.reference.IDatasetReference;
import com.okworx.ilcd.validation.reference.ReferenceCache;
import com.okworx.ilcd.validation.util.AbstractDatasetsTask;
import com.okworx.ilcd.validation.util.ILCDNameSpaceContext;
import com.okworx.ilcd.validation.util.PartitionedList;
import com.okworx.ilcd.validation.util.TaskResult;
import com.okworx.ilcd.validation.util.TransletCache;

/**
 * Checks whether local references (links) within datasets are valid.
 * 
 * @author oliver.kusche
 * 
 */
public class LinkValidator extends AbstractReferenceObjectsAwareValidator implements IValidator {

	public static final String PARAM_SKIP_REFS_TO_LCIAMETHODS = "skipReferencesToLCIAMethods";

	public static final String PARAM_SKIP_COMPLEMENTINGPROCESS = "skipComplementingProcess";

	public static final String PARAM_SKIP_REFERENCE_OBJECTS = "skipReferenceObjects";

	@Override
	public String getAspectName() {
		return "Links";
	}

	public String getAspectDescription() {
		return "Checks whether local references (links) within datasets are valid.";
	}

	protected ReferenceCache referenceObjectsCache;

	// TODO refactor out link extraction routine
	public boolean validate() throws InterruptedException {
		if (super.validate() == true) {
			log.debug("skipping validation");
			return true;
		}

		updateStatusValidating();

		this.unitsTotal = this.objectsToValidate.size();

		log.debug("initializing reference objects cache...");
		this.referenceObjectsCache = new ReferenceCache();
		this.referenceObjectsCache.put(this.objectsToValidate);
		log.debug("done - " + this.referenceObjectsCache.getLinks().size() + " objects in cache.");

		boolean result = true;

		try {

			XPathExpression expr = setupXpathRef();

			PartitionedList<IDatasetReference> partList = new PartitionedList<IDatasetReference>(
					this.objectsToValidate.values());

			log.debug(this.unitsTotal + " total objects split into " + partList.getPartitions().size() + " chunks");

			Collection<Callable<TaskResult>> tasks = new ArrayList<Callable<TaskResult>>();

			for (List<IDatasetReference> refList : partList.getPartitions()) {
				tasks.add(new LinkExtractorTask(refList, TransletCache.getInstance().getTranslet(), expr, this));
			}

			ExecutorService executor = Executors.newFixedThreadPool(partList.getNumThreads());

			try {
				List<Future<TaskResult>> taskResults = executor.invokeAll(tasks);
				for (Future<TaskResult> taskResult : taskResults) {
					if (taskResult.get() != null) {
						TaskResult res = taskResult.get();
						this.eventsList.addAll(res.getValidationEvents());
						this.statistics.add(res.getStatistics());
					}
				}
				executor.shutdown();
			} catch (InterruptedException e) {
				executor.shutdown();
				interrupted(e);
			} catch (Exception e) {
				log.error(e);
			}

			log.info(this.eventsList.getEvents().size() + " events");

			updateProgress(1);
			updateStatusDone();

			return this.getEventsList().isPositive();

		}

		catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	private XPathExpression setupXpathRef() throws XPathExpressionException {
		XPathFactory xpathFactory = XPathFactory.newInstance();
		XPath xpath = xpathFactory.newXPath();
		xpath.setNamespaceContext(new ILCDNameSpaceContext());
		XPathExpression expr = xpath.compile("/*/*");
		return expr;
	}

	final class LinkExtractorTask extends AbstractDatasetsTask implements Callable<TaskResult> {

		private XPathExpression expr;

		private Templates templates;

		LinkExtractorTask(List<IDatasetReference> files, Templates templates, XPathExpression expr,
				LinkValidator validator) {
			this.files = files;
			this.expr = expr;
			this.templates = templates;
			this.validator = validator;
		}

		public TaskResult call() throws Exception {
			return new TaskResult(extract(files), this.statistics);
		}

		private Collection<IValidationEvent> extract(Collection<IDatasetReference> files) throws Exception {

			Collection<IValidationEvent> events = new ArrayList<IValidationEvent>();

			Transformer transformer = templates.newTransformer();

			int count = 0;

			for (IDatasetReference reference : files) {
				if (Thread.currentThread().isInterrupted()) {
					log.info("operation was interrupted, aborting");
					updateStatusCancelled();
					break;
				}

				int eventCount = events.size();
				
				if (!reference.getType().equals(DatasetType.EXTERNAL_FILE))
					checkReference(events, transformer, reference);

				boolean success = (eventCount == events.size());
				this.statistics.update(reference, success);
				
				if (success && LinkValidator.this.reportSuccesses)
					events.add(new ValidationEvent(LinkValidator.this.getAspectName(), Severity.SUCCESS, reference, ValidationEvent.SUCCESS_MESSAGE));

				count = updateChunkCount(count);
			}

			return events;
		}

		private void checkReference(Collection<IValidationEvent> events, Transformer transformer,
				IDatasetReference reference) throws TransformerException, XPathExpressionException, IOException {

			NodeList refs = extractNodeList(transformer, reference);

			List<IDatasetReference> links = new ArrayList<IDatasetReference>();

			for (int i = 0; i < refs.getLength(); i++) {
				Node ref = refs.item(i);
				String uuid = ref.getAttributes().getNamedItem("refObjectId").getNodeValue().trim();
				String uri = ref.getAttributes().getNamedItem("uri").getNodeValue().trim();

				// skip remote links
				if (StringUtils.startsWithIgnoreCase(uri.trim(), "http://"))
					continue;

				String type = ref.getAttributes().getNamedItem("type").getNodeValue();
				DatasetType dsType = null;
				try {
					dsType = DatasetType.fromValue(type);
				} catch (Exception e) {
					log.error("invalid dataset type " + type);
				}
				links.add(new DatasetReference(uuid, uri, dsType, null,
						dsType.equals(DatasetType.EXTERNAL_FILE) ? FilenameUtils.getName(uri.trim()) : null));
			}

			log.debug("checking " + links.size() + " links");

			boolean skipReferenceObjects = (this.validator.parameters.get(LinkValidator.PARAM_SKIP_REFERENCE_OBJECTS) != null);
			
			for (IDatasetReference ref : links) {
				if (!LinkValidator.this.referenceObjectsCache.contains(ref) && !ref.equals(reference)) {
					
					// if skipReferenceObjects param is set, check whether it's listed there
					if (skipReferenceObjects && ((LinkValidator) this.validator).referenceObjects.containsKey(ref.getUuid())) {
						if (log.isDebugEnabled())
							log.debug("ignoring object found in reference list: " + ref.getUuid());
						continue;
					}
					
					// check whether we might have an external document
					if (ref.getType().equals(DatasetType.EXTERNAL_FILE)) {
						if (log.isDebugEnabled())
							log.debug(ref.getShortFileName()
								+ " "
								+ LinkValidator.this.referenceObjectsCache.getLinks().keySet()
										.contains(ref.getShortFileName()));
						if (LinkValidator.this.referenceObjectsCache.getLinks().keySet()
								.contains(ref.getShortFileName()))
							continue;
					}

					String messagePrefix = "could not resolve reference to object ";
					String messageSuffix = " (" + ref.getType().getValue() + ")";
					
					if (StringUtils.isBlank(ref.getUuid()) && StringUtils.isBlank(ref.getUri())) {
						messagePrefix = "Invalid (because empty) reference to " +  ref.getType().getValue() + ", neither UUID nor URI specified";
						messageSuffix = "";
					}

					events.add(new ValidationEvent(LinkValidator.this.getAspectName(), Severity.ERROR, reference,
							messagePrefix + (StringUtils.isNotBlank(ref.getUuid()) ? ref.getUuid() : ref.getUri())
									+ messageSuffix));
				}
			}
		}

		private NodeList extractNodeList(Transformer transformer, IDatasetReference reference)
				throws TransformerException, XPathExpressionException, IOException {
			DOMResult transformResult = new DOMResult();
			
			Object paramSkipSiam = this.validator.getParameter(PARAM_SKIP_REFS_TO_LCIAMETHODS);
			if (paramSkipSiam != null) {
				if (LinkValidator.this.log.isDebugEnabled())
					LinkValidator.this.log.debug("setting skipSupportedImpactAssessmentMethods=true");
				transformer.setParameter(PARAM_SKIP_REFS_TO_LCIAMETHODS, new Boolean(true));
			}

			Object paramSkipCP = this.validator.getParameter(PARAM_SKIP_COMPLEMENTINGPROCESS);
			if (paramSkipCP != null) {
				if (LinkValidator.this.log.isDebugEnabled())
					LinkValidator.this.log.debug("setting skipComplementingProcess=true");
				transformer.setParameter(PARAM_SKIP_COMPLEMENTINGPROCESS, new Boolean(true));
			}

			transformer.transform(new StreamSource(new TFileInputStream(reference.getAbsoluteFileName())),
					transformResult);

			Document resultDoc = (Document) transformResult.getNode();

			NodeList refs = (NodeList) expr.evaluate(resultDoc, XPathConstants.NODESET);
			return refs;
		}
	}
}
