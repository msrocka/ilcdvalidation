package com.okworx.ilcd.validation.reference;

import com.okworx.ilcd.validation.common.DatasetType;

public interface IDatasetReference {

	public abstract String getUuid();

	public abstract String getVersion();

	public abstract String getUri();

	public abstract String getName();
	
	public abstract String getAbsoluteFileName();

	public abstract String getShortFileName();

	public abstract boolean equals(Object obj);

	public abstract DatasetType getType();

	public abstract void setType(DatasetType type);

	public abstract void setName(String name);

}