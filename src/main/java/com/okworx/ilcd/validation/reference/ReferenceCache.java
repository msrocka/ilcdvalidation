package com.okworx.ilcd.validation.reference;

import java.io.File;
import java.net.URI;
import java.util.Collection;
import java.util.HashMap;

import org.apache.log4j.Logger;

public class ReferenceCache {

	protected final Logger log = org.apache.log4j.Logger.getLogger(this
			.getClass());

	private HashMap<String, IDatasetReference> references = new HashMap<String, IDatasetReference>();

	public HashMap<String, IDatasetReference> getLinks() {
		return references;
	}
	
	public void put(HashMap<String, IDatasetReference> objects) {
		this.references.putAll( objects );
	}

	/**
	 * 
	 * @param directory
	 */
	public void initializeFromDir(File directory) {

		ReferenceBuilder builder = new ReferenceBuilder();

		builder.build(directory);

		this.references = builder.getReferences();

	}

	public void initializeFromFiles(Collection<File> files) {
	}

	public void initializeFromURIs(Collection<URI> files) {
	}

	public void initializeFromArchive(File archive) {
		initializeFromDir(archive);
	}

	public boolean contains(IDatasetReference reference) {
		return references.containsKey(reference.getUuid());
	}

	public boolean contains(String uuid) {
		return references.containsKey(uuid);
	}
}
