package com.okworx.ilcd.validation.reference;

import org.apache.commons.lang3.StringUtils;

import com.okworx.ilcd.validation.common.DatasetType;

public class DatasetReference implements IDatasetReference {

	private String uuid;

	private String version;

	private String uri;

	private String name;

	private String absoluteFileName;

	private String shortFileName;

	private DatasetType type;

	public DatasetReference(String uuid, DatasetType type) {
		this(uuid, null, null, type, null, null);
	}
	
	public DatasetReference(String uuid, String version, DatasetType type) {
		this(uuid, version, null, type, null, null);
	}
	
	public DatasetReference(String uuid, String version, String absoluteFileName, String shortFileName) {
		this.uuid = uuid;
		this.version = version;
		this.absoluteFileName = absoluteFileName;
		this.shortFileName = shortFileName;
	}

	public DatasetReference(String absoluteFileName, String shortFileName) {
		this(null, null, null, null, absoluteFileName, shortFileName);
	}
	
	public DatasetReference(String uuid, String uri, DatasetType type, String absoluteFileName, String shortFileName) {
		this(uuid, null, uri, type, absoluteFileName, shortFileName);
	}

	public DatasetReference(String uuid, String version, String uri, String absoluteFileName, String shortFileName) {
		this(uuid, version, uri, null, absoluteFileName, shortFileName);
	}

	public DatasetReference(String uuid, String version, String uri, DatasetType type, String absoluteFileName, String shortFileName) {
		this.uuid = uuid;
		this.version = version;
		this.uri = uri;
		this.type = type;
		this.absoluteFileName = absoluteFileName;
		this.shortFileName = shortFileName;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getAbsoluteFileName() {
		return absoluteFileName;
	}

	public void getAbsoluteFileName(String fileName) {
		this.absoluteFileName = fileName;
	}

	public String getShortFileName() {
		return shortFileName;
	}

	public void setShortFileName(String shortFileName) {
		this.shortFileName = shortFileName;
	}

	public DatasetType getType() {
		return this.type;
	}

	public void setType(DatasetType type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof IDatasetReference))
			return false;
		IDatasetReference other = (IDatasetReference) obj;
		
		// compare uuid, if set
		if ( (this.uuid == null && other.getUuid() != null) || (this.uuid != null && !this.uuid.equalsIgnoreCase( other.getUuid() )) ) {
			return false;
		}

		// compare version, if set
		if ( (this.version == null && other.getVersion() != null) || (this.version != null && !this.version.equals( other.getVersion())) ) {
			return false;
		}

		// compare absolute file name, if set
		if ( (this.absoluteFileName == null && other.getAbsoluteFileName() != null) || (this.absoluteFileName != null && !this.absoluteFileName.equals( other.getAbsoluteFileName())) ) {
			return false;
		}

		return true;
	}
	
	public int hashCode() {
		int hashCode = 31;
		if (StringUtils.isNotEmpty(this.uuid))
			hashCode += this.uuid.toLowerCase().hashCode();
		if (StringUtils.isNotEmpty(this.absoluteFileName))
			hashCode += this.absoluteFileName.hashCode();
		if (StringUtils.isNotEmpty(this.name))
			hashCode += this.name.hashCode();
		if (StringUtils.isNotEmpty(this.version))
			hashCode += this.version.hashCode();
		if (this.type != null)
			hashCode += this.type.hashCode();
		return hashCode;
	}

	public String toString() {
		StringBuilder builder = new StringBuilder();
		if (this.type != null)
			builder.append(this.type).append(" ");
		if (StringUtils.isNotEmpty(this.uuid))
			builder.append(this.uuid.toLowerCase()).append(" ");
		if (StringUtils.isNotEmpty(this.version))
			builder.append(this.version).append(" ");
		if (StringUtils.isNotEmpty(this.name))
			builder.append(this.name).append(" ");
		if (StringUtils.isNotEmpty(this.absoluteFileName))
			builder.append(this.absoluteFileName).append(" ");
		builder.append(hashCode());
		return builder.toString();
	}
}
