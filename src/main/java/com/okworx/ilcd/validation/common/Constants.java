package com.okworx.ilcd.validation.common;

public class Constants {

	public static final String DEFAULT_PROFILE_JAR = "profiles/ILCD_1.1_profile_1.6a.jar";

	public static final String[] DEFAULT_SECONDARY_PROFILE_JARS = { "profiles/ILCD_1.1_EL_profile_1.0.3a.jar",
			"profiles/EF_profile_1.0.4.jar" };

	/*
	 * paths
	 */
	public static final String ILCD_PATH_PREFIX = "eu/europa/ec/jrc/lca/ilcd/";

	public static final String ILCD_SCHEMAS_PATH_PREFIX = ILCD_PATH_PREFIX + "schemas/";

	public static final String STYLESHEETS_PATH_PREFIX = ILCD_PATH_PREFIX + "stylesheets/";

	public static final String VALIDATE_STYLESHEET_NAME = "validate.xsl";

	public static final String VALIDATE_STYLESHEET = STYLESHEETS_PATH_PREFIX + "validate.xsl";

	public static final String DEFAULT_ILCD_LOCATIONS_NAME = "ILCDLocations.xml";

	public static final String DEFAULT_ILCD_LCIA_METHODOLOGIES_NAME = "ILCDLCIAMethodologies.xml";

	public static final String DEFAULT_ILCD_CATEGORIES_NAME = "ILCDClassification.xml";

	public static final String DEFAULT_ILCD_FLOW_CATEGORIES_NAME = "ILCDFlowCategorization.xml";

	public static final String REFERENCE_CATEGORIES_FILE_NAME = "ILCDClassification_Reference.xml";

	public static final String REFERENCE_FLOW_CATEGORIES_FILE_NAME = "ILCDFlowCategorization_Reference.xml";

	public static final String REFERENCE_CATEGORIES = STYLESHEETS_PATH_PREFIX + REFERENCE_CATEGORIES_FILE_NAME;

	public static final String REFERENCE_LOCATIONS_FILE_NAME = "ILCDLocations_Reference.xml";

	public static final String REFERENCE_LOCATIONS = STYLESHEETS_PATH_PREFIX + REFERENCE_LOCATIONS_FILE_NAME;

	public static final String REFERENCE_LCIA_METHODOLOGIES_FILE_NAME = "ILCDLCIAMethodologies_Reference.xml";

	public static final String REFERENCE_LCIA_METHODOLOGIES = STYLESHEETS_PATH_PREFIX
			+ REFERENCE_LCIA_METHODOLOGIES_FILE_NAME;

	/*
	 * namespaces
	 */
	public static final String NS_SCHEMA_INSTANCE = "http://www.w3.org/2001/XMLSchema-instance";

	public static final String NS_PROCESS = "http://lca.jrc.it/ILCD/Process";

	public static final String NS_LCIAMETHOD = "http://lca.jrc.it/ILCD/LCIAMethod";

	public static final String NS_FLOW = "http://lca.jrc.it/ILCD/Flow";

	public static final String NS_FLOWPROPERTY = "http://lca.jrc.it/ILCD/FlowProperty";

	public static final String NS_UNITGROUP = "http://lca.jrc.it/ILCD/UnitGroup";

	public static final String NS_SOURCE = "http://lca.jrc.it/ILCD/Source";

	public static final String NS_CONTACT = "http://lca.jrc.it/ILCD/Contact";

	public static final String NS_COMMON = "http://lca.jrc.it/ILCD/Common";

	/*
	 * root elements
	 */
	public static final String LOCATIONS_ROOT_ELEMENT_NAME = "ILCDLocations";

	public static final String LCIAMETHODOLOGIES_ROOT_ELEMENT_NAME = "ILCDLCIAMethodologies";

	public static final String CATEGORIES_ROOT_ELEMENT_NAME = "CategorySystem";

	public static final String LCIAMETHOD_ROOT_ELEMENT_NAME = "LCIAMethodDataSet";

	public static final String CONTACT_ROOT_ELEMENT_NAME = "contactDataSet";

	public static final String SOURCE_ROOT_ELEMENT_NAME = "sourceDataSet";

	public static final String UNIT_GROUP_ROOT_ELEMENT_NAME = "unitGroupDataSet";

	public static final String FLOW_PROPERTY_ROOT_ELEMENT_NAME = "flowPropertyDataSet";

	public static final String FLOW_ROOT_ELEMENT_NAME = "flowDataSet";

	public static final String PROCESS_ROOT_ELEMENT_NAME = "processDataSet";

	/*
	 * folder names
	 */
	public static final String FOLDER_NAME_ARCHIVE_ROOT = "ILCD";

	public static final String FOLDER_NAME_CONTACT = "contacts";

	public static final String FOLDER_NAME_SOURCE = "sources";

	public static final String FOLDER_NAME_UNIT_GROUP = "unitgroups";

	public static final String FOLDER_NAME_FLOW_PROPERTY = "flowproperties";

	public static final String FOLDER_NAME_FLOW = "flows";

	public static final String FOLDER_NAME_LCIA_METHOD = "lciamethods";

	public static final String FOLDER_NAME_PROCESS = "processes";

	/*
	 * schema names
	 */
	public static final String PROCESS_SCHEMA_NAME = "ILCD_ProcessDataSet.xsd";

	public static final String LCIAMETHOD_SCHEMA_NAME = "ILCD_LCIAMethodDataSet.xsd";

	public static final String FLOW_SCHEMA_NAME = "ILCD_FlowDataSet.xsd";

	public static final String FLOW_PROPERTY_SCHEMA_NAME = "ILCD_FlowPropertyDataSet.xsd";

	public static final String UNIT_GROUP_SCHEMA_NAME = "ILCD_UnitGroupDataSet.xsd";

	public static final String SOURCE_SCHEMA_NAME = "ILCD_SourceDataSet.xsd";

	public static final String CONTACT_SCHEMA_NAME = "ILCD_ContactDataSet.xsd";

	/*
	 * schemas
	 */
	public static final String PROCESS_SCHEMA = ILCD_SCHEMAS_PATH_PREFIX + PROCESS_SCHEMA_NAME;

	public static final String LCIAMETHOD_SCHEMA = ILCD_SCHEMAS_PATH_PREFIX + LCIAMETHOD_SCHEMA_NAME;

	public static final String FLOW_SCHEMA = ILCD_SCHEMAS_PATH_PREFIX + FLOW_SCHEMA_NAME;

	public static final String FLOW_PROPERTY_SCHEMA = ILCD_SCHEMAS_PATH_PREFIX + FLOW_PROPERTY_SCHEMA_NAME;

	public static final String UNIT_GROUP_SCHEMA = ILCD_SCHEMAS_PATH_PREFIX + UNIT_GROUP_SCHEMA_NAME;

	public static final String SOURCE_SCHEMA = ILCD_SCHEMAS_PATH_PREFIX + SOURCE_SCHEMA_NAME;

	public static final String CONTACT_SCHEMA = ILCD_SCHEMAS_PATH_PREFIX + CONTACT_SCHEMA_NAME;

}
