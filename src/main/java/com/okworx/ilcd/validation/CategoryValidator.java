package com.okworx.ilcd.validation;

import com.okworx.ilcd.validation.profile.Profile;
import com.okworx.ilcd.validation.util.PrefixBuilder;

/**
 * Validates category information in datasets - checks whether the declared
 * categories comply with the specified categories file.
 * 
 * @author oliver.kusche
 * 
 * @TODO implement category checking functionality
 *
 */
public class CategoryValidator extends XSLTStylesheetValidator implements IValidator {

	public static final String CATEGORIES = "CATEGORIES";

	protected boolean mandatoryClassification = false;
	
	@Override
	public String getAspectName() {
		return "Categories";
	}

	public String getAspectDescription() {
		return "Checks whether the declared categories comply with the specified categories file.";
	}
	
	public boolean isMandatoryClassification() {
		return this.mandatoryClassification;
	}
	
	public void setMandatoryClassification(boolean mandatoryClassification) {
		this.mandatoryClassification = mandatoryClassification;
		this.setTransformationParameter("noEmptyClassification", this.mandatoryClassification);
	}

	@Override
	public void setProfile(Profile profile) {
		super.setProfile(profile);
		if ( profile != null ) {
			registerStylesheet(profile.getPath().toString(), profile.getStylesheetsPath(), "validate-categories.xsl");
			this.setTransformationParameter("categoriesFile", profile.getCategoriesFile());
			this.resolverMappings.put(profile.getCategoriesFile(),
					PrefixBuilder.buildPath(profile.getPath().toString(), profile.getCategoriesFile()));
		}
	}
}
