package com.okworx.ilcd.validation.util;

public interface IUpdateEventListener {
	
	public void updateProgress(double percentFinished);

	public void updateStatus(String statusMessage);

}
