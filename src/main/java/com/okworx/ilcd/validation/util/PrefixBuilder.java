package com.okworx.ilcd.validation.util;

import org.apache.commons.lang3.StringUtils;

public class PrefixBuilder {
	public static String buildPrefix(String pathToJar, String resourcePath) {
		// build "jar:file:" + pathToJar + "!/" + resourcePath;
		StringBuffer buf = new StringBuffer(buildPath(pathToJar, resourcePath));
		if (StringUtils.isNotBlank(pathToJar) && StringUtils.isNotBlank(resourcePath) && !resourcePath.endsWith("/"))
			buf.append("/");
		return buf.toString();
	}

	public static String buildPath(String pathToJar, String resourcePath) {
		// build "jar:file:" + pathToJar + "!/" + resourcePath;
		StringBuffer buf = new StringBuffer("jar:file:");
		buf.append(pathToJar);
		buf.append("!/");
		buf.append(resourcePath);
		return buf.toString();
	}
}
