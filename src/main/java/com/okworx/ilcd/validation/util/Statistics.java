package com.okworx.ilcd.validation.util;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;

import com.okworx.ilcd.validation.reference.IDatasetReference;

public class Statistics {

	protected final Logger log = org.apache.log4j.Logger.getLogger(this.getClass());

	protected Set<IDatasetReference> validProcesses = new HashSet<IDatasetReference>();

	protected Set<IDatasetReference> validFlows = new HashSet<IDatasetReference>();

	protected Set<IDatasetReference> validFlowProperties = new HashSet<IDatasetReference>();

	protected Set<IDatasetReference> validUnitGroups = new HashSet<IDatasetReference>();

	protected Set<IDatasetReference> validSources = new HashSet<IDatasetReference>();

	protected Set<IDatasetReference> validContacts = new HashSet<IDatasetReference>();

	protected Set<IDatasetReference> validLCIAMethods = new HashSet<IDatasetReference>();

	protected Set<IDatasetReference> validExternalFiles = new HashSet<IDatasetReference>();

	protected Set<IDatasetReference> invalidProcesses = new HashSet<IDatasetReference>();

	protected Set<IDatasetReference> invalidFlows = new HashSet<IDatasetReference>();

	protected Set<IDatasetReference> invalidFlowProperties = new HashSet<IDatasetReference>();

	protected Set<IDatasetReference> invalidUnitGroups = new HashSet<IDatasetReference>();

	protected Set<IDatasetReference> invalidSources = new HashSet<IDatasetReference>();

	protected Set<IDatasetReference> invalidContacts = new HashSet<IDatasetReference>();

	protected Set<IDatasetReference> invalidLCIAMethods = new HashSet<IDatasetReference>();

	protected Set<IDatasetReference> invalidExternalFiles = new HashSet<IDatasetReference>();

	public void update(IDatasetReference reference, boolean success) {
		if (log.isTraceEnabled())
			log.trace("counting " + (success ? "" : "un") + "successful validation of " + reference.getType());

		switch (reference.getType()) {
		case CONTACT:
			if (success) {
				this.validContacts.add(reference);
			} else {
				this.invalidContacts.add(reference);
				this.validContacts.remove(reference);
			}
			break;
		case EXTERNAL_FILE:
			if (success) {
				this.validExternalFiles.add(reference);
			} else {
				this.invalidExternalFiles.add(reference);
				this.validExternalFiles.remove(reference);
			}
			break;
		case FLOW:
			if (success) {
				this.validFlows.add(reference);
			} else {
				this.invalidFlows.add(reference);
				this.validFlows.remove(reference);
			}
			break;
		case FLOWPROPERTY:
			if (success) {
				this.validFlowProperties.add(reference);
			} else {
				this.invalidFlowProperties.add(reference);
				this.validFlowProperties.remove(reference);
			}
			break;
		case LCIAMETHOD:
			if (success) {
				this.validLCIAMethods.add(reference);
			} else {
				this.invalidLCIAMethods.add(reference);
				this.validLCIAMethods.remove(reference);
			}
			break;
		case PROCESS:
			if (success) {
				this.validProcesses.add(reference);
			} else {
				this.invalidProcesses.add(reference);
				this.validProcesses.remove(reference);
			}
			break;
		case SOURCE:
			if (success) {
				this.validSources.add(reference);
			} else {
				this.invalidSources.add(reference);
				this.validSources.remove(reference);
			}
			break;
		case UNITGROUP:
			if (success) {
				this.validUnitGroups.add(reference);
			} else {
				this.invalidUnitGroups.add(reference);
				this.validUnitGroups.remove(reference);
			}
			break;
		}
	}

	/**
	 * This method combines two statistics by merely adding them.
	 * Use only for statistics from sets of disjunct data. 
	 * 
	 * @param stats
	 */
	public void add(Statistics stats) {
		this.validContacts.addAll(stats.getValidContacts());
		this.validFlowProperties.addAll(stats.getValidFlowProperties());
		this.validFlows.addAll(stats.getValidFlows());
		this.validLCIAMethods.addAll(stats.getValidLCIAMethods());
		this.validProcesses.addAll(stats.getValidProcesses());
		this.validSources.addAll(stats.getValidSources());
		this.validUnitGroups.addAll(stats.getValidUnitGroups());
		this.validExternalFiles.addAll(stats.getValidExternalFiles());
		
		this.invalidContacts.addAll(stats.getInvalidContacts());
		this.invalidFlowProperties.addAll(stats.getInvalidFlowProperties());
		this.invalidFlows.addAll(stats.getInvalidFlows());
		this.invalidLCIAMethods.addAll(stats.getInvalidLCIAMethods());
		this.invalidProcesses.addAll(stats.getInvalidProcesses());
		this.invalidSources.addAll(stats.getInvalidSources());
		this.invalidUnitGroups.addAll(stats.getInvalidUnitGroups());
		this.invalidExternalFiles.addAll(stats.getInvalidExternalFiles());
	}

	/**
	 * Merges the results of two Statistics. As if a dataset has been found 
	 * invalid once, its status cannot revert back to valid, thus the number
	 * of valid datasets will be constant or reduced with each merge operation
	 * but never growing. 
	 * 
	 * @param stats
	 */
	public void merge(Statistics stats) {

		this.invalidContacts.addAll(stats.getInvalidContacts());
		this.invalidFlowProperties.addAll(stats.getInvalidFlowProperties());
		this.invalidFlows.addAll(stats.getInvalidFlows());
		this.invalidLCIAMethods.addAll(stats.getInvalidLCIAMethods());
		this.invalidProcesses.addAll(stats.getInvalidProcesses());
		this.invalidSources.addAll(stats.getInvalidSources());
		this.invalidUnitGroups.addAll(stats.getInvalidUnitGroups());
		this.invalidExternalFiles.addAll(stats.getInvalidExternalFiles());

		this.validContacts.removeAll(stats.getInvalidContacts());
		this.validFlowProperties.removeAll(stats.getInvalidFlowProperties());
		this.validFlows.removeAll(stats.getInvalidFlows());
		this.validLCIAMethods.removeAll(stats.getInvalidLCIAMethods());
		this.validProcesses.removeAll(stats.getInvalidProcesses());
		this.validSources.removeAll(stats.getInvalidSources());
		this.validUnitGroups.removeAll(stats.getInvalidUnitGroups());
		this.validExternalFiles.removeAll(stats.getInvalidExternalFiles());
	}
	
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Total files processed: ").append(this.getTotalValidCount() + this.getTotalInvalidCount());
		builder.append(System.lineSeparator());
		builder.append("Process datasets found valid: ").append(this.getValidProcessesCount()).append(" / invalid: ").append(this.getInvalidProcessesCount());
		builder.append(System.lineSeparator());
		builder.append("Flow datasets found valid: ").append(this.getValidFlowsCount()).append(" / invalid: ").append(this.getInvalidFlowsCount());
		builder.append(System.lineSeparator());
		builder.append("Source datasets found valid: ").append(this.getValidSourcesCount()).append(" / invalid: ").append(this.getInvalidSourcesCount());
		builder.append(System.lineSeparator());
		builder.append("Contact datasets found valid: ").append(this.getValidContactsCount()).append(" / invalid: ").append(this.getInvalidContactsCount());
		builder.append(System.lineSeparator());
		builder.append("LCIA Method datasets found valid: ").append(this.getValidLCIAMethodsCount()).append(" / invalid: ").append(this.getInvalidLCIAMethodsCount());
		builder.append(System.lineSeparator());
		builder.append("Flow Property datasets found valid: ").append(this.getValidFlowPropertiesCount()).append(" / invalid: ").append(this.getInvalidFlowPropertiesCount());
		builder.append(System.lineSeparator());
		builder.append("Unit Group datasets found valid: ").append(this.getValidUnitGroupsCount()).append(" / invalid: ").append(this.getInvalidUnitGroupsCount());
		builder.append(System.lineSeparator());
		builder.append("External files found valid: ").append(this.getValidExternalFilesCount()).append(" / invalid: ").append(this.getInvalidExternalFilesCount());
		builder.append(System.lineSeparator());
		
		return builder.toString();
	}

	public int getTotalProcessesCount() {
		return validProcesses.size() + invalidProcesses.size();
	}

	public int getTotalFlowsCount() {
		return validFlows.size() + invalidFlows.size();
	}

	public int getTotalFlowPropertiesCount() {
		return validFlowProperties.size() + invalidFlowProperties.size();
	}

	public int getTotalUnitGroupsCount() {
		return validUnitGroups.size() + invalidUnitGroups.size();
	}

	public int getTotalSourcesCount() {
		return validSources.size() + invalidSources.size();
	}

	public int getTotalContactsCount() {
		return validContacts.size() + invalidContacts.size();
	}

	public int getTotalLCIAMethodsCount() {
		return validLCIAMethods.size() + invalidLCIAMethods.size();
	}

	public int getTotalExternalFilesCount() {
		return validExternalFiles.size() + invalidExternalFiles.size();
	}

	public int getValidProcessesCount() {
		return validProcesses.size();
	}

	public int getValidFlowsCount() {
		return validFlows.size();
	}

	public int getValidFlowPropertiesCount() {
		return validFlowProperties.size();
	}

	public int getValidUnitGroupsCount() {
		return validUnitGroups.size();
	}

	public int getValidSourcesCount() {
		return validSources.size();
	}

	public int getValidContactsCount() {
		return validContacts.size();
	}

	public int getValidLCIAMethodsCount() {
		return validLCIAMethods.size();
	}

	public int getValidExternalFilesCount() {
		return validExternalFiles.size();
	}

	public int getInvalidProcessesCount() {
		return invalidProcesses.size();
	}

	public int getInvalidFlowsCount() {
		return invalidFlows.size();
	}

	public int getInvalidFlowPropertiesCount() {
		return invalidFlowProperties.size();
	}

	public int getInvalidUnitGroupsCount() {
		return invalidUnitGroups.size();
	}

	public int getInvalidSourcesCount() {
		return invalidSources.size();
	}

	public int getInvalidContactsCount() {
		return invalidContacts.size();
	}

	public int getInvalidLCIAMethodsCount() {
		return invalidLCIAMethods.size();
	}

	public int getInvalidExternalFilesCount() {
		return invalidExternalFiles.size();
	}

	public int getTotalValidCount() {
		return validContacts.size() + validFlowProperties.size() + validFlows.size() + validLCIAMethods.size()
				+ validProcesses.size() + validSources.size() + validUnitGroups.size() + validExternalFiles.size();
	}

	public int getTotalInvalidCount() {
		return invalidContacts.size() + invalidFlowProperties.size() + invalidFlows.size() + invalidLCIAMethods.size()
				+ invalidProcesses.size() + invalidSources.size() + invalidUnitGroups.size() + invalidExternalFiles.size();
	}
	
	public int getValidDatasetsCount() {
		return validContacts.size() + validFlowProperties.size() + validFlows.size() + validLCIAMethods.size()
				+ validProcesses.size() + validSources.size() + validUnitGroups.size();
	}

	public int getInvalidDatasetsCount() {
		return invalidContacts.size() + invalidFlowProperties.size() + invalidFlows.size() + invalidLCIAMethods.size()
				+ invalidProcesses.size() + invalidSources.size() + invalidUnitGroups.size();
	}
	
	public int getTotalCount() {
		return getTotalValidCount() + getTotalInvalidCount();
	}
	
	public int getTotalDatasetsCount() {
		return getValidDatasetsCount() + getInvalidDatasetsCount();
	}
	
	public Set<IDatasetReference> getValidProcesses() {
		return validProcesses;
	}

	public void setValidProcesses(Set<IDatasetReference> validProcesses) {
		this.validProcesses = validProcesses;
	}

	public Set<IDatasetReference> getValidFlows() {
		return validFlows;
	}

	public void setValidFlows(Set<IDatasetReference> validFlows) {
		this.validFlows = validFlows;
	}

	public Set<IDatasetReference> getValidFlowProperties() {
		return validFlowProperties;
	}

	public void setValidFlowProperties(Set<IDatasetReference> validFlowProperties) {
		this.validFlowProperties = validFlowProperties;
	}

	public Set<IDatasetReference> getValidUnitGroups() {
		return validUnitGroups;
	}

	public void setValidUnitGroups(Set<IDatasetReference> validUnitGroups) {
		this.validUnitGroups = validUnitGroups;
	}

	public Set<IDatasetReference> getValidSources() {
		return validSources;
	}

	public void setValidSources(Set<IDatasetReference> validSources) {
		this.validSources = validSources;
	}

	public Set<IDatasetReference> getValidContacts() {
		return validContacts;
	}

	public void setValidContacts(Set<IDatasetReference> validContacts) {
		this.validContacts = validContacts;
	}

	public Set<IDatasetReference> getValidLCIAMethods() {
		return validLCIAMethods;
	}

	public void setValidLCIAMethods(Set<IDatasetReference> validLCIAMethods) {
		this.validLCIAMethods = validLCIAMethods;
	}

	public Set<IDatasetReference> getInvalidProcesses() {
		return invalidProcesses;
	}

	public void setInvalidProcesses(Set<IDatasetReference> invalidProcesses) {
		this.invalidProcesses = invalidProcesses;
	}

	public Set<IDatasetReference> getInvalidFlows() {
		return invalidFlows;
	}

	public void setInvalidFlows(Set<IDatasetReference> invalidFlows) {
		this.invalidFlows = invalidFlows;
	}

	public Set<IDatasetReference> getInvalidFlowProperties() {
		return invalidFlowProperties;
	}

	public void setInvalidFlowProperties(Set<IDatasetReference> invalidFlowProperties) {
		this.invalidFlowProperties = invalidFlowProperties;
	}

	public Set<IDatasetReference> getInvalidUnitGroups() {
		return invalidUnitGroups;
	}

	public void setInvalidUnitGroups(Set<IDatasetReference> invalidUnitGroups) {
		this.invalidUnitGroups = invalidUnitGroups;
	}

	public Set<IDatasetReference> getInvalidSources() {
		return invalidSources;
	}

	public void setInvalidSources(Set<IDatasetReference> invalidSources) {
		this.invalidSources = invalidSources;
	}

	public Set<IDatasetReference> getInvalidContacts() {
		return invalidContacts;
	}

	public void setInvalidContacts(Set<IDatasetReference> invalidContacts) {
		this.invalidContacts = invalidContacts;
	}

	public Set<IDatasetReference> getInvalidLCIAMethods() {
		return invalidLCIAMethods;
	}

	public void setInvalidLCIAMethods(Set<IDatasetReference> invalidLCIAMethods) {
		this.invalidLCIAMethods = invalidLCIAMethods;
	}

	public Set<IDatasetReference> getValidExternalFiles() {
		return validExternalFiles;
	}

	public void setValidExternalFiles(Set<IDatasetReference> validExtFiles) {
		this.validExternalFiles = validExtFiles;
	}

	public Set<IDatasetReference> getInvalidExternalFiles() {
		return invalidExternalFiles;
	}

	public void setInvalidExternalFiles(Set<IDatasetReference> invalidExtFiles) {
		this.invalidExternalFiles = invalidExtFiles;
	}

}
