package com.okworx.ilcd.validation.util;

import javax.xml.transform.Templates;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;

public class TransletCache {

	private static final TransletCache instance = new TransletCache();

	private Templates translet;

	public Templates getTranslet() {
		return this.translet;
	}

	protected TransletCache() {
		TransformerFactory fact = TransformerFactory.newInstance();
		try {
			this.translet = fact.newTemplates(new StreamSource(this.getClass()
					.getClassLoader()
					.getResourceAsStream("stylesheets/extractReferences.xsl")));
		} catch (TransformerConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static TransletCache getInstance() {
		return instance;
	}
}
