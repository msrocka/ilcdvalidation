package com.okworx.ilcd.validation.util;

import java.io.IOException;
import java.net.URL;

public interface Locator {

	URL resolve(URL url) throws IOException;

}
