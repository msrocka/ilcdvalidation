package com.okworx.ilcd.validation.util;

import java.util.Collection;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.okworx.ilcd.validation.SchemaValidator;
import com.okworx.ilcd.validation.events.IValidationEvent;
import com.okworx.ilcd.validation.events.Severity;
import com.okworx.ilcd.validation.events.ValidationEvent;
import com.okworx.ilcd.validation.reference.IDatasetReference;

public class SchemaErrorHandler implements ErrorHandler {

	private IDatasetReference reference;
	
	private SchemaValidator validator;

	private ConcurrentLinkedQueue<IValidationEvent> events = new ConcurrentLinkedQueue<IValidationEvent>();

	public SchemaErrorHandler(SchemaValidator schemaValidator) {
		this.validator = schemaValidator;
	}

	public Collection<IValidationEvent> getEvents() {
		return events;
	}

	public IDatasetReference getReference() {
		return reference;
	}

	public void setReference(IDatasetReference reference) {
		this.reference = reference;
	}

	public void error(SAXParseException arg0) throws SAXException {
		this.events.add(new ValidationEvent(this.validator.getAspectName(), Severity.ERROR, this.reference, arg0.getLineNumber() + ","
				+ arg0.getColumnNumber() + " " + arg0.getMessage()));
	}

	public void fatalError(SAXParseException arg0) throws SAXException {
		this.events.add(new ValidationEvent(this.validator.getAspectName(), Severity.ERROR, this.reference, arg0.getMessage()));
	}

	public void warning(SAXParseException arg0) throws SAXException {
		this.events.add(new ValidationEvent(this.validator.getAspectName(), Severity.WARNING, this.reference, arg0.getMessage()));

	}

}
