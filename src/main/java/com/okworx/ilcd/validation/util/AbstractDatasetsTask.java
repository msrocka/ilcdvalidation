package com.okworx.ilcd.validation.util;

import java.util.Collection;

import org.apache.log4j.Logger;

import com.okworx.ilcd.validation.AbstractDatasetsValidator;
import com.okworx.ilcd.validation.reference.IDatasetReference;

public class AbstractDatasetsTask {

	protected final Logger log = org.apache.log4j.Logger.getLogger(this.getClass());
	
	protected AbstractDatasetsValidator validator;
	
	protected Collection<IDatasetReference> files;
	
	protected Statistics statistics = new Statistics();

	protected int updateChunkCount(int count) {
		count++;
		if ((count % this.validator.getUpdateInterval() == 0)) {
			this.validator.triggerProgressUpdate(count);
			count = 0;
		}
		return count;
	}

}
