package com.okworx.ilcd.validation.util;

import java.util.Iterator;

import javax.xml.namespace.NamespaceContext;

import com.okworx.ilcd.validation.common.Constants;

public class ILCDNameSpaceContext implements NamespaceContext {

	public String getNamespaceURI(String prefix) {

		String namespaceUri = null;

		if (prefix.equals("common"))
			namespaceUri = Constants.NS_COMMON;

		if (prefix.equals("p"))
			namespaceUri = Constants.NS_PROCESS;

		else if (prefix.equals("l"))
			namespaceUri = Constants.NS_LCIAMETHOD;

		else if (prefix.equals("f"))
			namespaceUri = Constants.NS_FLOW;

		else if (prefix.equals("fp"))
			namespaceUri = Constants.NS_FLOWPROPERTY;

		else if (prefix.equals("u"))
			namespaceUri = Constants.NS_UNITGROUP;

		else if (prefix.equals("s"))
			namespaceUri = Constants.NS_SOURCE;

		else if (prefix.equals("c"))
			namespaceUri = Constants.NS_CONTACT;

		return namespaceUri;
	}

	public String getPrefix(String namespaceUri) {
		String prefix = null;

		if (namespaceUri.equals(Constants.NS_COMMON))
			prefix = "common";

		else if (namespaceUri.equals(Constants.NS_PROCESS))
			prefix = "p";

		else if (namespaceUri.equals(Constants.NS_LCIAMETHOD))
			prefix = "l";

		else if (namespaceUri.equals(Constants.NS_FLOW))
			prefix = "f";

		else if (namespaceUri.equals(Constants.NS_FLOWPROPERTY))
			prefix = "fp";

		else if (namespaceUri.equals(Constants.NS_UNITGROUP))
			prefix = "u";

		else if (namespaceUri.equals(Constants.NS_SOURCE))
			prefix = "s";

		else if (namespaceUri.equals(Constants.NS_CONTACT))
			prefix = "c";

		return prefix;
	}

	public Iterator<String> getPrefixes(String arg0) {
		return null;
	}

}
