package com.okworx.ilcd.validation.util;

import java.util.Collection;

import com.okworx.ilcd.validation.events.IValidationEvent;

public class TaskResult {

	private Collection<IValidationEvent> validationEvents;
	
	private Statistics statistics;

	public TaskResult(Collection<IValidationEvent> events, Statistics stats) {
		this.validationEvents = events;
		this.statistics = stats;
	}

	public Collection<IValidationEvent> getValidationEvents() {
		return validationEvents;
	}

	public void setValidationEvents(Collection<IValidationEvent> validationEvents) {
		this.validationEvents = validationEvents;
	}

	public Statistics getStatistics() {
		return statistics;
	}

	public void setStatistics(Statistics statistics) {
		this.statistics = statistics;
	}
}
