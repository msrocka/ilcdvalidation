package com.okworx.ilcd.validation.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.GZIPOutputStream;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

public class ReferenceListCreator {

	protected final Logger log = org.apache.log4j.Logger.getLogger(this.getClass());

	public static void main(String[] args) {

		ReferenceListCreator rlc = new ReferenceListCreator();

		// rlc.test();

//		rlc.processXLS("/Users/oliver.kusche/git/ilcdvalidation/src/main/reference_data/ILCD-ref-ElemenFlows-August-2016_with_exceptions4PEF_update1.xls");
		rlc.processXLSv2("/Users/oliver.kusche/git/ilcdvalidation/src/main/reference_data/EF-LCIAMethod_CF(V1.8.12).xls");

	}

	private void serialize(HashMap<String, String> map, String file) {
		try {
			log.info("serializing map with " + map.size() + " entries");
			FileOutputStream fos = new FileOutputStream(file);
			GZIPOutputStream gz = new GZIPOutputStream(fos);
			ObjectOutputStream oos = new ObjectOutputStream(gz);
			oos.writeObject(map);
			oos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void processXLS(String file) {
		try {
			Workbook wbk = Workbook.getWorkbook(new File(file));

			Sheet sheet = wbk.getSheet(0);

			int rows = sheet.getRows();

			HashMap<String, String> map = new HashMap<String, String>();

			for (int row = 3; row < rows; row++) {
				String uuid = sheet.getCell(8, row).getContents().toLowerCase();

				if (StringUtils.isBlank(uuid)) {
					log.warn("skipping empty UUID in line " + row);
					continue;
				}
				
				String casNo = sheet.getCell(0, row).getContents();
				String name = sheet.getCell(1, row).getContents();
				String category = sheet.getCell(5, row).getContents().concat("/")
						.concat(sheet.getCell(6, row).getContents()).concat("/")
						.concat(sheet.getCell(7, row).getContents());

				String data = name.concat("; ").concat(casNo).concat("; ").concat(category);

				if (row==3||row==rows-1)
					log.info(uuid.concat(" ").concat(data));

				if (map.containsKey(uuid))
					log.warn("found duplicate key " + uuid);

				map.put(uuid, data);
			}

			log.info(" processed " + (rows - 3) + " rows");

//			serialize(map,
//					"src/main/profiles/ILCD_1.1_profile_1.6a.contents/eu/europa/ec/jrc/lca/ilcd/reference/ILCD_ref_elementary_flows_august_2016.ser.z");

			serialize(map,
					"src/main/profiles/ILCD_1.1_EL_profile_1.0.3d.contents/eu/europa/ec/jrc/lca/ilcd/reference/ILCD_ref_elementary_flows_august_2016_with_exceptions4PEF_update1.ser.z");

//			serialize(map,
//					"src/main/profiles/EF_profile_1.0.3.contents/eu/europa/ec/jrc/lca/ilcd/reference/ILCD_ref_elementary_flows_may_2016_rev1.ser.z");

		} catch (BiffException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.info("done.");
	}

	private void processXLSv2(String file) {
		try {
			Workbook wbk = Workbook.getWorkbook(new File(file));

			HashMap<String, String> map = new HashMap<String, String>();

			// read sheet with elem. flows
			readSheet(wbk.getSheet(0), map);
			
			// read sheet with other flows
			readSheet(wbk.getSheet(1), map);

//			serialize(map,
//					"src/main/profiles/ILCD_1.1_profile_1.6.contents/eu/europa/ec/jrc/lca/ilcd/reference/ILCD_ref_elementary_flows_august_2016.ser.z");
//
//			serialize(map,
//					"src/main/profiles/ILCD_1.1_EL_profile_1.0.3.contents/eu/europa/ec/jrc/lca/ilcd/reference/ILCD_ref_elementary_flows_august_2016.ser.z");

			serialize(map,
					"src/main/profiles/EF_profile_1.0.9.contents/eu/europa/ec/jrc/lca/ilcd/reference/EF_elementary_flows_06_2017_rev1.8.12.ser.z");

		} catch (BiffException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	private void readSheet(Sheet sheet, Map<String,String> map) {
		int rows = sheet.getRows();

		log.info(rows + " rows found in sheet " + sheet.getName());

		for (int row = 1; row < rows; row++) {
			String uuid = sheet.getCell(0, row).getContents();

			if (StringUtils.isBlank(uuid)) {
				log.warn("end of document reached in line " + row);
				break;
			}

			String casNo = sheet.getCell(2, row).getContents();
			String name = sheet.getCell(1, row).getContents();
			String category = sheet.getCell(5, row).getContents().concat("/")
					.concat(sheet.getCell(6, row).getContents()).concat("/")
					.concat(sheet.getCell(7, row).getContents());

			String data = name.concat("; ").concat(casNo).concat("; ").concat(category);

			if (row==1||row==rows-1)
				log.info(uuid.concat(" ").concat(data));

			if (map.containsKey(uuid))
				log.warn("found duplicate key " + uuid);

			map.put(uuid, data);
		}

		log.info(" processed " + (rows - 1) + " rows");
	}

	private void test() {

		HashMap<String, String> map = new HashMap<String, String>();

		map.put("15e6da4c-ace4-42a3-abff-9138ac65a7e6",
				"(+)-bornan-2-one; 000464-49-3; Emissions/Emissions to soil/Emissions to agricultural soil");
		map.put("16b7ee1b-798a-46e2-bd79-e2b74c715bfb",
				"(+)-bornan-2-one; 000464-49-3; Emissions/Emissions to soil/Emissions to urban air close to ground");
		map.put("1d10edcf-c1b8-470c-8f20-311f25b4bab8",
				"(+)-bornan-2-one; 000464-49-3; Emissions/Emissions to soil/Emissions to non-urban air or from high stacks");

		serialize(map,
				"src/main/resources/profiles/ILCD_1.1_profile_1.1.contents/eu/europa/ec/jrc/lca/ilcd/reference/elementary_flows.ser.z");

	}

}
