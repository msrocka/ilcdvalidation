package com.okworx.ilcd.validation;

/**
 * Validates a set of given datasets for matching UUID and filename pattern.
 * 
 * @author oliver.kusche
 *
 */
public class FilenameValidator extends AbstractDatasetsValidator implements IValidator {

	@Override
	public String getAspectName() {
		return "TFile names";
	}

	public String getAspectDescription() {
		return "Checks for matching UUID and filename pattern.";
	}

	public boolean validate() {
		// TODO Auto-generated method stub
		return false;
	}

}
