package com.okworx.ilcd.validation;

import java.io.File;
import java.util.HashMap;

import com.okworx.ilcd.validation.profile.Profile;
import com.okworx.ilcd.validation.reference.IDatasetReference;
import com.okworx.ilcd.validation.reference.ReferenceBuilder;
import com.okworx.ilcd.validation.util.Statistics;

public abstract class AbstractDatasetsValidator extends AbstractValidator implements IValidator, IDatasetsValidator {

	protected double unitsDone = 0;

	protected double unitsTotal = 0;

	protected int updateInterval = 5;

	protected boolean validateArchives = false;

	protected HashMap<String, IDatasetReference> objectsToValidate;

	protected Profile profile = null;

	protected Statistics statistics = new Statistics();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.okworx.ilcd.validation.IDatasetsValidator#setObjectsToValidate(java
	 * .util.HashMap)
	 */
	public void setObjectsToValidate(HashMap<String, IDatasetReference> objects) {
		this.objectsToValidate = objects;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.okworx.ilcd.validation.IDatasetsValidator#setObjectsToValidate(java
	 * .io.File)
	 */
	public void setObjectsToValidate(File source) {

		ReferenceBuilder builder = new ReferenceBuilder(this.getAspectName());

		this.objectsToValidate = builder.build(source);

		this.eventsList.addAll(builder.getEventsList().getEvents());
		
		if (this.validateArchives) {
			// check whether source is directory, and only otherwise call
			// validateArchive()
			if (!source.isDirectory() && !source.getName().toLowerCase().endsWith(".xml"))
				validateArchive(source);
		}
	}

	private void validateArchive(File source) {
		ArchiveValidator av = new ArchiveValidator();
		av.setArchiveToValidate(source);
		av.validate();
		this.eventsList.addAll(av.getEventsList().getEvents());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.okworx.ilcd.validation.IDatasetsValidator#getObjectsToValidate()
	 */
	public HashMap<String, IDatasetReference> getObjectsToValidate() {
		return this.objectsToValidate;
	}

	public void reset() {
		super.reset();
		this.objectsToValidate = null;
		if (this.updateEventListener != null)
			this.updateEventListener.updateProgress(0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.okworx.ilcd.validation.IDatasetsValidator#validate()
	 */
	@Override
	public boolean validate() throws InterruptedException {
		log.info("validating");

		if (this.objectsToValidate == null)
			throw new IllegalArgumentException();

		if (this.objectsToValidate.isEmpty()) {
			log.debug("nothing to validate");
			return this.eventsList.isPositive();
		}

		if (log.isDebugEnabled())
			for (String paramName : this.parameters.keySet())
				log.debug("using parameter " + paramName + " : " + this.parameters.get(paramName));

		return false;
	}

	/**
	 * Assign a specific profile to this validator which may contain XML
	 * schemas, validation stylesheets, categories, reference nomenclature etc.
	 * that will be automatically registered with the validator.
	 * 
	 * @param profile
	 *            the profile
	 */
	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	public synchronized void triggerProgressUpdate(int count) {
		this.unitsDone += count;
		if (log.isTraceEnabled())
			log.trace(count + " / " + unitsDone + " / " + unitsTotal + " / " + (unitsDone / unitsTotal));
		updateProgress(unitsDone / unitsTotal);
	}
	
	public int getUpdateInterval() {
		return updateInterval;
	}

	public void setUpdateInterval(int updateInterval) {
		this.updateInterval = updateInterval;
	}

	public boolean isValidateArchives() {
		return validateArchives;
	}

	public void setValidateArchives(boolean validateArchives) {
		this.validateArchives = validateArchives;
	}

	public Statistics getStatistics() {
		return statistics;
	}

	public void setStatistics(Statistics statistics) {
		this.statistics = statistics;
	}

	public Profile getProfile() {
		return profile;
	}

}
