package com.okworx.ilcd.validation;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.okworx.ilcd.validation.events.EventsList;
import com.okworx.ilcd.validation.util.IUpdateEventListener;

public abstract class AbstractValidator implements IValidator {

	protected final Logger log = org.apache.log4j.Logger.getLogger(this.getClass());

	public abstract String getAspectName();

	public abstract String getAspectDescription();

	protected EventsList eventsList = new EventsList(this.getAspectName());

	public abstract boolean validate() throws InterruptedException;
	
	protected IUpdateEventListener updateEventListener;

	protected boolean reportSuccesses = false;

	protected Map<String, Object> parameters = new HashMap<String, Object>();
	
	public EventsList getEventsList() {
		return eventsList;
	}

	public void setParameter(String parameterName, Object obj) {
		if (obj != null)
			this.parameters.put(parameterName, obj);
		else
			this.parameters.remove(parameterName);
	}

	public Object getParameter(String parameterName) {
		return this.parameters.get(parameterName);
	}

	public void reset() {
		this.eventsList = new EventsList(this.getAspectName());
		this.parameters = new HashMap<String, Object>();
	}

	public AbstractValidator() {
	}
	
	public IUpdateEventListener getUpdateEventListener() {
		return updateEventListener;
	}

	public void setUpdateEventListener(
			IUpdateEventListener updateEventListener) {
		this.updateEventListener = updateEventListener;
	}

	protected void updateProgress(double percentFinished) {
		if (this.getUpdateEventListener() != null)
			this.getUpdateEventListener().updateProgress(percentFinished);
	}

	protected void updateStatus(String message) {
		if (this.getUpdateEventListener() != null)
			this.getUpdateEventListener().updateStatus(message);
	}

	protected void updateStatusSetup() {
		updateStatus("Setting up...");
	}

	protected void updateStatusValidating() {
		updateStatus("Validating...");
	}

	protected void updateStatusDone() {
		updateStatus("Done.");
	}

	protected void updateStatusCancelled() {
		updateStatus("Cancelled");
	}
	
	protected void interrupted(InterruptedException e) throws InterruptedException {
		log.info("operation was interrupted, aborting");
		updateStatusCancelled();
		updateProgress(0);
		throw e;
	}

	protected void interrupted() throws InterruptedException {
		interrupted(new InterruptedException());
	}

	public boolean isReportSuccesses() {
		return reportSuccesses;
	}

	public void setReportSuccesses(boolean reportSuccesses) {
		this.reportSuccesses = reportSuccesses;
	}
	
}
