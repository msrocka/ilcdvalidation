package com.okworx.ilcd.validation.profile;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.jar.Attributes;
import java.util.jar.JarFile;
import java.util.jar.Manifest;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.okworx.ilcd.validation.common.Constants;
import com.okworx.ilcd.validation.exception.InvalidProfileException;
import com.okworx.ilcd.validation.util.Locator;
import com.okworx.ilcd.validation.util.StandaloneLocator;

/**
 * This purpose of this class is to hold references to various profiles that
 * might be used for validation. See the Profiles section in the documentation
 * for more information on profiles.
 * 
 * Example for passing parameters during initialization:
 * 
 * new ProfileManager.ProfileManagerBuilder().cacheDir(File dir).locator(new
 * EclipseLocator()).build();
 * 
 */
public enum ProfileManager {

	INSTANCE;

	private final Logger log = org.apache.log4j.Logger.getLogger(this.getClass());

	private Map<URL, Profile> profileStore = new HashMap<URL, Profile>();

	private Locator locator = new StandaloneLocator();

	private File cacheDir = new File(org.apache.commons.io.FileUtils.getTempDirectoryPath() + File.separator
			+ System.currentTimeMillis());

	private boolean regDefault = false;

	private URL defaultProfileURL;

	ProfileManager() {
		init();
	}

	private void init() {
		try {

			this.defaultProfileURL = this.getClass().getClassLoader().getResource(Constants.DEFAULT_PROFILE_JAR)
					.toURI().toURL();

			if (log.isTraceEnabled()) {
				log.trace("registering default profile at " + locator.resolve(this.defaultProfileURL).toString());
				log.trace("using cache dir " + this.cacheDir.getPath());
				log.trace("using locator " + this.locator.getClass().getCanonicalName());
			}

			if (!this.cacheDir.exists()) {
				if (log.isTraceEnabled()) {
					log.trace("cache dir " + this.cacheDir.getAbsolutePath() + " does not exist. Creating...");
				}
				FileUtils.forceMkdir(this.cacheDir);
				FileUtils.forceDeleteOnExit(this.cacheDir);
			}

			// register default profile
			registerProfile(this.defaultProfileURL);

			// if loading of default secondary profiles is configured, register them as well
			if (isRegDefault()) {
				if (log.isDebugEnabled())
					log.debug("Registering secondary default profiles");
				for (String profilePath : Constants.DEFAULT_SECONDARY_PROFILE_JARS) {
					URL url = this.getClass().getClassLoader().getResource(profilePath).toURI().toURL();
					registerProfile(url);
				}
			}

		} catch (Exception e) {
			log.error(e);
			e.printStackTrace();
		}
	}

	public Profile registerProfile(URL url) throws InvalidProfileException {

		log.debug("registering profile at " + url.toString());

		try {
			log.trace("resolving to " + locator.resolve(url));

			File physicalJar = extractJar(locator.resolve(url));

			JarFile jar = new JarFile(physicalJar);

			Manifest m = jar.getManifest();

			Profile profile = new Profile(url);

			Attributes atts = m.getAttributes("ILCD-Validator-Profile");

			if (atts == null) {
				jar.close();
				throw new InvalidProfileException();
			}

			profile.setName(atts.getValue("Profile-Name"));
			profile.setVersion(atts.getValue("Profile-Version"));

			profile.setCategoriesFile(atts.getValue("Profile-Categories"));

			profile.setSchemasPath(atts.getValue("Profile-SchemasPath"));
			String[] schemas = extractCommaSeparatedValues(atts.getValue("Profile-Schemas"));
			profile.setSchemas(schemas);

			profile.setStylesheetsPath(atts.getValue("Profile-StylesheetsPath"));
			profile.setValidationStylesheet(atts.getValue("Profile-ValidationStylesheet"));

			String reference = atts.getValue("Profile-Reference");
			String refElementaryFlows = atts.getValue("Profile-ReferenceElementaryFlows");
			if (StringUtils.isNotEmpty(reference) && StringUtils.isNotEmpty(refElementaryFlows))
				profile.setReferenceElementaryFlows(reference.concat("/").concat(refElementaryFlows));

			profile.setJarFile(jar);
			profile.setPath(physicalJar);

			this.profileStore.put(url, profile);

			return profile;

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
		}
		return null;

	}

	public void deregisterProfile(Profile profile) {
		this.profileStore.remove(profile.getURL());
	}

	private File extractJar(URL path) {
		try {

			String plainName = FilenameUtils.getName(path.getFile());

			File extractedJar = new File(this.cacheDir.getAbsolutePath() + File.separator + plainName);

			FileUtils.copyInputStreamToFile(path.openStream(), extractedJar);

			return extractedJar;
		} catch (Exception ex) {
			ex.printStackTrace();
			log.error(ex);
		}

		return null;
	}

	public String[] extractCommaSeparatedValues(String csList) {
		StringTokenizer t = new StringTokenizer(csList, ",");

		int entries = t.countTokens();

		String[] result = new String[entries];

		int i = 0;
		while (t.hasMoreElements()) {
			result[i] = (String) t.nextElement();
			i++;
		}

		return result;
	}

	public Collection<Profile> getProfiles() {
		return this.profileStore.values();
	}

	public Profile getDefaultProfile() {
		return this.profileStore.get(this.defaultProfileURL);
	}

	public Locator getLocator() {
		return locator;
	}

	public void setLocator(Locator locator) {
		this.locator = locator;
	}

	public File getCacheDir() {
		return cacheDir;
	}

	public void setCacheDir(File cacheDir) {
		this.cacheDir = cacheDir;
	}

	private void build(ProfileManagerBuilder builder) {
		this.locator = builder.locator;
		this.cacheDir = builder.cacheDir;
		this.regDefault = builder.regDefault;
		init();
	}

	public static ProfileManager getInstance() {
		return INSTANCE;
	}

	public boolean isRegDefault() {
		return regDefault;
	}

	public void setRegDefault(boolean regDefault) {
		this.regDefault = regDefault;
	}

	/**
	 * Use this to build an instance of ProfileManager with a custom
	 * configuration
	 *
	 */
	public static class ProfileManagerBuilder {

		private Locator locator = new StandaloneLocator();
		private File cacheDir = new File(org.apache.commons.io.FileUtils.getTempDirectoryPath() + File.separator
				+ System.currentTimeMillis());
		private boolean regDefault = false;

		public ProfileManagerBuilder() {
		}

		public ProfileManagerBuilder locator(Locator locator) {
			this.locator = locator;
			return this;
		}

		public ProfileManagerBuilder cacheDir(File cacheDir) {
			this.cacheDir = cacheDir;
			return this;
		}

		public ProfileManagerBuilder registerDefaultProfiles(boolean regDefault) {
			this.regDefault = regDefault;
			return this;
		}

		public void build() {
			ProfileManager.INSTANCE.build(this);
		}
	}
}
