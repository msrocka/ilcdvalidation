package com.okworx.ilcd.validation.profile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.jar.JarFile;

import com.okworx.ilcd.validation.util.PrefixBuilder;

public class Profile {

	private URL jarUrl;

	private File path;
	
	private String name;
	private String version;

	private String schemasPath;
	private String[] schemas;

	private String categoriesFile;

	private String stylesheetsPath;
	private String validationStylesheet;

	private String referenceElementaryFlows;

	private JarFile jarFile;

	public Profile(URL jarUrl) {
		this.jarUrl = jarUrl;
	}

	public InputStream getResourceAsStream(String name) throws IOException {
		return jarFile.getInputStream(jarFile.getEntry(name));
	}

	public String getURLPrefix(String resourcePath) {
		return PrefixBuilder.buildPrefix(this.jarUrl.toString(), resourcePath);
	}

	public String getURLPrefix() {
		return PrefixBuilder.buildPrefix(this.jarUrl.toString(), "");
	}

	public URL getURL() {
		return jarUrl;
	}

	public void setURL(URL jarUrl) {
		this.jarUrl = jarUrl;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSchemasPath() {
		return schemasPath;
	}

	public void setSchemasPath(String schemasPath) {
		this.schemasPath = schemasPath;
	}

	public String[] getSchemas() {
		return schemas;
	}

	public void setSchemas(String[] schemas) {
		this.schemas = schemas;
	}

	public String getCategoriesFile() {
		return categoriesFile;
	}

	public void setCategoriesFile(String categoriesFile) {
		this.categoriesFile = categoriesFile;
	}

	public String getStylesheetsPath() {
		return stylesheetsPath;
	}

	public void setStylesheetsPath(String stylesheetsPath) {
		this.stylesheetsPath = stylesheetsPath;
	}

	public String getValidationStylesheet() {
		return validationStylesheet;
	}

	public void setValidationStylesheet(String validationStylesheet) {
		this.validationStylesheet = validationStylesheet;
	}

	public JarFile getJarFile() {
		return jarFile;
	}

	public void setJarFile(JarFile jarFile) {
		this.jarFile = jarFile;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public File getPath() {
		return path;
	}

	public void setPath(File path) {
		this.path = path;
	}

	public String getReferenceElementaryFlows() {
		return referenceElementaryFlows;
	}

	public void setReferenceElementaryFlows(String referenceElementaryFlows) {
		this.referenceElementaryFlows = referenceElementaryFlows;
	}

}
