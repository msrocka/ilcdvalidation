package com.okworx.ilcd.validation;


/**
 * Validates category information in datasets.
 * 
 * @author oliver.kusche
 *
 */
public class ILCDFormatValidator extends ValidatorChain implements IValidator {

	@Override
	public String getAspectName() {
		return "ILCD Format Validity";
	}

	public ILCDFormatValidator() {

		AbstractDatasetsValidator sv = setupSchemaValidator();

		XSLTStylesheetValidator xslv = setupXSLValidator();

		this.validators.add(sv);
		this.validators.add(xslv);

	}

	private XSLTStylesheetValidator setupXSLValidator() {
		XSLTStylesheetValidator xslv = new XSLTStylesheetValidator();

		xslv.setAspectName("ILCD Format Validity");

//		xslv.setProfile(ProfileManager.INSTANCE.getDefaultProfile());
//		xslv.registerStylesheet(this.getClass().getClassLoader().getResource(Constants.DEFAULT_PROFILE_JAR).getPath(), Constants.STYLESHEETS_PATH_PREFIX, Constants.VALIDATE_STYLESHEET_NAME);
		
		return xslv;
	}

	private AbstractDatasetsValidator setupSchemaValidator() {
		SchemaValidator sv = new SchemaValidator();

//		sv.registerDefaultSchemas();
		
		return sv;
	}
}
