<?xml version="1.0" encoding="UTF-8"?>
<!-- ILCD Format Version 1.1_SNAPSHOT Tools Build 1016 -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:exslt="http://exslt.org/common" xmlns:categories="http://lca.jrc.it/ILCD/Categories"
    xmlns:locations="http://lca.jrc.it/ILCD/Locations" xmlns:lciamethodologies="http://lca.jrc.it/ILCD/LCIAMethodologies" xmlns:process="http://lca.jrc.it/ILCD/Process"
    xmlns:lciamethod="http://lca.jrc.it/ILCD/LCIAMethod" xmlns:flow="http://lca.jrc.it/ILCD/Flow" xmlns:flowproperty="http://lca.jrc.it/ILCD/FlowProperty"
    xmlns:unitgroup="http://lca.jrc.it/ILCD/UnitGroup" xmlns:source="http://lca.jrc.it/ILCD/Source" xmlns:contact="http://lca.jrc.it/ILCD/Contact"
    xmlns:common="http://lca.jrc.it/ILCD/Common">

    <xsl:import href="common.xsl"/>
    
     <xsl:template name="checkFieldsProcessEF">
        
         <xsl:call-template name="checkDQI">
             <xsl:with-param name="indicatorName" select="'Technological representativeness'"/>
         </xsl:call-template>
         <xsl:call-template name="checkDQI">
             <xsl:with-param name="indicatorName" select="'Time representativeness'"/>
         </xsl:call-template>
         <xsl:call-template name="checkDQI">
             <xsl:with-param name="indicatorName" select="'Geographical representativeness'"/>
         </xsl:call-template>
         <xsl:call-template name="checkDQI">
             <xsl:with-param name="indicatorName" select="'Completeness'"/>
         </xsl:call-template>
         <xsl:call-template name="checkDQI">
             <xsl:with-param name="indicatorName" select="'Precision'"/>
         </xsl:call-template>
         <xsl:call-template name="checkDQI">
             <xsl:with-param name="indicatorName" select="'Methodological appropriateness and consistency'"/>
         </xsl:call-template>
         <xsl:call-template name="checkDQI">
             <xsl:with-param name="indicatorName" select="'Overall quality'"/>
         </xsl:call-template>
         
    </xsl:template>


    <xsl:template name="checkDQI">
        <xsl:param name="indicatorName"/>
        
        <xsl:call-template name="checkPresence2">
            <xsl:with-param name="element" select="process:modellingAndValidation/process:validation/process:review[@type='Accredited third party review' or @type='Independent external review' or @type='Independent review panel']/common:dataQualityIndicators/common:dataQualityIndicator[@name=$indicatorName]/@value"/>
            <xsl:with-param name="elementDesc" select="concat('Data Quality Indicator for ', $indicatorName)"/>
            <xsl:with-param name="suppressDefaultPrefix" select="'true'"/>
        </xsl:call-template>
        
        <!-- todo: check individually for each of those reviews -->
        <xsl:variable name="DQIvalue" select="process:modellingAndValidation/process:validation/process:review[@type='Accredited third party review' or @type='Independent external review' or @type='Independent review panel']/common:dataQualityIndicators/common:dataQualityIndicator[@name=$indicatorName]/@value"/>
        
        <xsl:choose>            
            <xsl:when test="$DQIvalue='Very good' or $DQIvalue='Good' or $DQIvalue='Fair'">
                <!-- we're good -->
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="validationEvent">
                    <xsl:with-param name="fancyMessages" select="$fancyMessages"/>
                    <xsl:with-param name="message">Data Quality Indicator for <xsl:value-of select="$indicatorName"/> must be either "Very good", "Good" or "Fair", but is "<xsl:value-of select="$DQIvalue"/>".</xsl:with-param>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>

        
    </xsl:template>

    
</xsl:stylesheet>
