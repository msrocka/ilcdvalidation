<?xml version="1.0" encoding="UTF-8"?>
<!-- ILCD Format Version 1.1_SNAPSHOT Tools Build 1016 -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:exslt="http://exslt.org/common" xmlns:categories="http://lca.jrc.it/ILCD/Categories"
    xmlns:locations="http://lca.jrc.it/ILCD/Locations" xmlns:lciamethodologies="http://lca.jrc.it/ILCD/LCIAMethodologies" xmlns:process="http://lca.jrc.it/ILCD/Process"
    xmlns:lciamethod="http://lca.jrc.it/ILCD/LCIAMethod" xmlns:flow="http://lca.jrc.it/ILCD/Flow" xmlns:flowproperty="http://lca.jrc.it/ILCD/FlowProperty"
    xmlns:unitgroup="http://lca.jrc.it/ILCD/UnitGroup" xmlns:source="http://lca.jrc.it/ILCD/Source" xmlns:contact="http://lca.jrc.it/ILCD/Contact"
    xmlns:common="http://lca.jrc.it/ILCD/Common" xmlns:epd="http://www.iai.kit.edu/EPD/2013" xmlns:matml="http://www.matml.org/">

    <xsl:import href="validate.xsl"/>

    <xsl:output indent="no" method="text"/>

    <xsl:variable name="version" select="'1.1'"/>

    <xsl:variable name="allowedModules" select="'|A1|A2|A3|A4|A5|A1-A3|B1|B2|B3|B4|B5|B6|B7|C1|C2|C3|C4|D|'"/>

    <xsl:variable name="fancyMessages" select="false()"/>

    <xsl:template match="/">
        <xsl:apply-templates/>
    </xsl:template>


    <xsl:template match="process:processDataSet">
        <xsl:call-template name="checkFieldsPresence"/>
        <xsl:call-template name="checkIndicatorPresence"/>
        <xsl:call-template name="checkModules"/>
    </xsl:template>

    <xsl:template match="flow:flowDataSet">
        <xsl:call-template name="checkFlowMaterialPropertiesPresence"/>
    </xsl:template>

    <xsl:template name="checkFlowMaterialPropertiesPresence">
        <xsl:variable name="matMLContent" select="/flow:flowDataSet/flow:flowInformation/flow:dataSetInformation/common:other/matml:MatML_Doc"/>
        
        <xsl:variable name="numberOfDeclaredProperties" select="count($matMLContent/matml:Metadata/matml:PropertyDetails)"/>
        
        <xsl:choose>
            <xsl:when test="$numberOfDeclaredProperties=0">
                <xsl:call-template name="validationEvent">
                    <xsl:with-param name="message">No material properties are declared.</xsl:with-param>
                </xsl:call-template>                              
            </xsl:when>
            <xsl:otherwise>
                <xsl:if test="count($matMLContent/matml:Metadata/matml:PropertyDetails/matml:Name[translate(text(),'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='gross density'])=0">
                    <xsl:call-template name="validationEvent">
                        <xsl:with-param name="type" select="'warning'"/>
                        <xsl:with-param name="message">Required material property 'gross density' is not declared.</xsl:with-param>
                    </xsl:call-template>                                                  
                </xsl:if>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    

    <xsl:template name="checkIndicatorPresence">
        <xsl:call-template name="checkLCIIndicator">
            <xsl:with-param name="uuid" select="'20f32be5-0398-4288-9b6d-accddd195317'"/>
            <xsl:with-param name="name" select="'PERE'"/>
        </xsl:call-template>
        <xsl:call-template name="checkLCIIndicator">
            <xsl:with-param name="uuid" select="'fb3ec0de-548d-4508-aea5-00b73bf6f702'"/>
            <xsl:with-param name="name" select="'PERM'"/>
        </xsl:call-template>
        <xsl:call-template name="checkLCIIndicator">
            <xsl:with-param name="uuid" select="'53f97275-fa8a-4cdd-9024-65936002acd0'"/>
            <xsl:with-param name="name" select="'PERT'"/>
        </xsl:call-template>
        <xsl:call-template name="checkLCIIndicator">
            <xsl:with-param name="uuid" select="'ac857178-2b45-46ec-892a-a9a4332f0372'"/>
            <xsl:with-param name="name" select="'PENRE'"/>
        </xsl:call-template>
        <xsl:call-template name="checkLCIIndicator">
            <xsl:with-param name="uuid" select="'1421caa0-679d-4bf4-b282-0eb850ccae27'"/>
            <xsl:with-param name="name" select="'PENRM'"/>
        </xsl:call-template>
        <xsl:call-template name="checkLCIIndicator">
            <xsl:with-param name="uuid" select="'06159210-646b-4c8d-8583-da9b3b95a6c1'"/>
            <xsl:with-param name="name" select="'PENRT'"/>
        </xsl:call-template>
        <xsl:call-template name="checkLCIIndicator">
            <xsl:with-param name="uuid" select="'c6a1f35f-2d09-4f54-8dfb-97e502e1ce92'"/>
            <xsl:with-param name="name" select="'SM'"/>
        </xsl:call-template>
        <xsl:call-template name="checkLCIIndicator">
            <xsl:with-param name="uuid" select="'64333088-a55f-4aa2-9a31-c10b07816787'"/>
            <xsl:with-param name="name" select="'RSF'"/>
        </xsl:call-template>
        <xsl:call-template name="checkLCIIndicator">
            <xsl:with-param name="uuid" select="'89def144-d39a-4287-b86f-efde453ddcb2'"/>
            <xsl:with-param name="name" select="'NRSF'"/>
        </xsl:call-template>
        <xsl:call-template name="checkLCIIndicator">
            <xsl:with-param name="uuid" select="'3cf952c8-f3a4-461d-8c96-96456ca62246'"/>
            <xsl:with-param name="name" select="'FW'"/>
        </xsl:call-template>
        <xsl:call-template name="checkLCIIndicator">
            <xsl:with-param name="uuid" select="'430f9e0f-59b2-46a0-8e0d-55e0e84948fc'"/>
            <xsl:with-param name="name" select="'HWD'"/>
        </xsl:call-template>
        <xsl:call-template name="checkLCIIndicator">
            <xsl:with-param name="uuid" select="'b29ef66b-e286-4afa-949f-62f1a7b4d7fa'"/>
            <xsl:with-param name="name" select="'NHWD'"/>
        </xsl:call-template>
        <xsl:call-template name="checkLCIIndicator">
            <xsl:with-param name="uuid" select="'3449546e-52ad-4b39-b809-9fb77cea8ff6'"/>
            <xsl:with-param name="name" select="'RWD'"/>
        </xsl:call-template>
        <xsl:call-template name="checkLCIIndicator">
            <xsl:with-param name="uuid" select="'a2b32f97-3fc7-4af2-b209-525bc6426f33'"/>
            <xsl:with-param name="name" select="'CRU'"/>
            <xsl:with-param name="negativeAllowed" select="true()"/>
        </xsl:call-template>
        <xsl:call-template name="checkLCIIndicator">
            <xsl:with-param name="uuid" select="'d7fe48a5-4103-49c8-9aae-b0b5dfdbd6ae'"/>
            <xsl:with-param name="name" select="'MFR'"/>
            <xsl:with-param name="negativeAllowed" select="true()"/>
        </xsl:call-template>
        <xsl:call-template name="checkLCIIndicator">
            <xsl:with-param name="uuid" select="'59a9181c-3aaf-46ee-8b13-2b3723b6e447'"/>
            <xsl:with-param name="name" select="'MER'"/>
            <xsl:with-param name="negativeAllowed" select="true()"/>
        </xsl:call-template>
        <xsl:call-template name="checkLCIIndicator">
            <xsl:with-param name="uuid" select="'4da0c987-2b76-40d6-9e9e-82a017aaaf29'"/>
            <xsl:with-param name="name" select="'EEE'"/>
            <xsl:with-param name="negativeAllowed" select="true()"/>
        </xsl:call-template>
        <xsl:call-template name="checkLCIIndicator">
            <xsl:with-param name="uuid" select="'98daf38a-7a79-46d3-9a37-2b7bd0955810'"/>
            <xsl:with-param name="name" select="'EET'"/>
            <xsl:with-param name="negativeAllowed" select="true()"/>
        </xsl:call-template>

        <xsl:call-template name="checkLCIAIndicator">
            <xsl:with-param name="uuid" select="'77e416eb-a363-4258-a04e-171d843a6460'"/>
            <xsl:with-param name="name" select="'GWP'"/>
            <xsl:with-param name="negativeAllowed" select="true()"/>
        </xsl:call-template>
        <xsl:call-template name="checkLCIAIndicator">
            <xsl:with-param name="uuid" select="'06dcd26f-025f-401a-a7c1-5e457eb54637'"/>
            <xsl:with-param name="name" select="'ODP'"/>
        </xsl:call-template>
        <xsl:call-template name="checkLCIAIndicator">
            <xsl:with-param name="uuid" select="'b4274add-93b7-4905-a5e4-2e878c4e4216'"/>
            <xsl:with-param name="name" select="'AP'"/>
        </xsl:call-template>
        <xsl:call-template name="checkLCIAIndicator">
            <xsl:with-param name="uuid" select="'f58827d0-b407-4ec6-be75-8b69efb98a0f'"/>
            <xsl:with-param name="name" select="'EP'"/>
        </xsl:call-template>
        <xsl:call-template name="checkLCIAIndicator">
            <xsl:with-param name="uuid" select="'1e84a202-dae6-42aa-9e9d-71ea48b8be00'"/>
            <xsl:with-param name="name" select="'POCP'"/>
            <xsl:with-param name="negativeAllowed" select="true()"/>
        </xsl:call-template>
        <xsl:call-template name="checkLCIAIndicator">
            <xsl:with-param name="uuid" select="'f7c73bb9-ab1a-4249-9c6d-379a0de6f67e'"/>
            <xsl:with-param name="name" select="'ADPE'"/>
        </xsl:call-template>
        <xsl:call-template name="checkLCIAIndicator">
            <xsl:with-param name="uuid" select="'804ebcdf-309d-4098-8ed8-fdaf2f389981'"/>
            <xsl:with-param name="name" select="'ADPF'"/>
        </xsl:call-template>
    </xsl:template>

    <xsl:template name="checkLCIIndicator">
        <xsl:param name="uuid"/>
        <xsl:param name="name"/>
        <xsl:param name="negativeAllowed" select="false()"/>
        <xsl:if test="count(process:exchanges/process:exchange/process:referenceToFlowDataSet[@refObjectId = $uuid]) = 0">
            <xsl:call-template name="validationEvent">
                <xsl:with-param name="message">LCI indicator <xsl:value-of select="$name"/> (<xsl:value-of select="$uuid"/>) is not declared.</xsl:with-param>
            </xsl:call-template>               
        </xsl:if>
        <xsl:for-each select="process:exchanges/process:exchange[process:referenceToFlowDataSet/@refObjectId = $uuid]/common:other/epd:amount[@epd:module='A1' or @epd:module='A2' or @epd:module='A3' or @epd:module='A1-A3']">
            <xsl:if test="$negativeAllowed!='true' and starts-with(., '-')">
                <xsl:call-template name="validationEvent">
                    <xsl:with-param name="type" select="'warning'"></xsl:with-param>
                    <xsl:with-param name="message">A forbidden negative value (<xsl:value-of select="."/>) is declared for module <xsl:value-of select="./@epd:module"/> for LCI indicator <xsl:value-of select="$name"/> (<xsl:value-of select="$uuid"/>).</xsl:with-param>
                </xsl:call-template>               
            </xsl:if>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="checkLCIAIndicator">
        <xsl:param name="uuid"/>
        <xsl:param name="name"/>
        <xsl:param name="negativeAllowed" select="false()"/>
        <xsl:if test="count(process:LCIAResults/process:LCIAResult/process:referenceToLCIAMethodDataSet[@refObjectId = $uuid]) = 0">
            <xsl:call-template name="validationEvent">
                <xsl:with-param name="message">LCIA indicator <xsl:value-of select="$name"/> (<xsl:value-of select="$uuid"/>) is not declared.</xsl:with-param>
            </xsl:call-template>               
        </xsl:if>
        <xsl:for-each select="process:LCIAResults/process:LCIAResult[process:referenceToLCIAMethodDataSet/@refObjectId = $uuid]/common:other/epd:amount[@epd:module='A1' or @epd:module='A2' or @epd:module='A3' or @epd:module='A1-A3']">
            <xsl:if test="$negativeAllowed!='true' and starts-with(., '-')">
                <xsl:call-template name="validationEvent">
                    <xsl:with-param name="type" select="'warning'"></xsl:with-param>
                    <xsl:with-param name="message">A forbidden negative value (<xsl:value-of select="."/>) is declared for module <xsl:value-of select="./@epd:module"/> for LCIA indicator <xsl:value-of select="$name"/> (<xsl:value-of select="$uuid"/>).</xsl:with-param>
                </xsl:call-template>               
            </xsl:if>
        </xsl:for-each>
    </xsl:template>


    <xsl:template name="checkModules">
        <xsl:for-each select="//common:other/epd:amount">
            <xsl:variable name="module" select="concat('|', @epd:module, '|')"/>
            <!-- check module name -->
            <xsl:choose>
                <xsl:when test="$module='||'">
                    <xsl:call-template name="validationEvent">
                        <xsl:with-param name="message">No module declared for value <xsl:value-of select="./text()"/></xsl:with-param>
                    </xsl:call-template>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:if test="not(contains($allowedModules, $module))">
                        <xsl:call-template name="validationEvent">
                            <xsl:with-param name="message">Module name '<xsl:value-of select="@epd:module"/>' is invalid. Only one of <xsl:value-of select="$allowedModules"/> is allowed.</xsl:with-param>                            
                        </xsl:call-template>
                    </xsl:if>
                </xsl:otherwise>
            </xsl:choose>
            
            <!-- if D is declared, also C must be declared -->
            <xsl:if test="@epd:module='D'">
                <xsl:if test="count(preceding-sibling::epd:amount[starts-with(@epd:module,'C')])=0 and count(following-sibling::epd:amount[starts-with(@epd:module,'C')])=0">
                    <xsl:call-template name="validationEvent">
                        <xsl:with-param name="message">A value for module D has been declared for indicator '<xsl:value-of select="parent::*/preceding-sibling::process:referenceToFlowDataSet/common:shortDescription[1]|parent::*/preceding-sibling::process:referenceToLCIAMethodDataSet/common:shortDescription[1]"/>', but none for module C which is required in this case.</xsl:with-param>                            
                    </xsl:call-template>
                </xsl:if>
            </xsl:if>
            
        </xsl:for-each>
    </xsl:template>



    <xsl:template name="checkFieldsPresence">
        <xsl:call-template name="checkPresence">
            <xsl:with-param name="element" select="process:processInformation/process:dataSetInformation/common:UUID"/>
            <xsl:with-param name="elementDesc" select="'UUID'"/>
            <xsl:with-param name="messagePrefix"/>
        </xsl:call-template>

        <xsl:call-template name="checkPresence">
            <xsl:with-param name="element" select="process:processInformation/process:dataSetInformation/process:name/process:baseName"/>
            <xsl:with-param name="elementDesc" select="'name'"/>
            <xsl:with-param name="messagePrefix"/>
        </xsl:call-template>

        <xsl:call-template name="checkPresence">
            <xsl:with-param name="element"
                select="process:processInformation/process:dataSetInformation/process:classificationInformation/common:classification[translate(@name, 'abcdefghijklmnopqrstuvwxyzàáâãäåæçèéêëìíîïðñòóôõöøùúûüýþÿžšœ', 'ABCDEFGHIJKLMNOPQRSTUVWXYZÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞŸŽŠŒ') = 'OEKOBAU.DAT']"/>
            <xsl:with-param name="elementDesc" select="'OEKOBAU.DAT categories'"/>
            <xsl:with-param name="suppressDefaultPrefix" select="'true'"/>
            <xsl:with-param name="messagePrefix"/>
        </xsl:call-template>

        <xsl:call-template name="checkPresence">
            <xsl:with-param name="element" select="process:processInformation/process:quantitativeReference/process:referenceToReferenceFlow"/>
            <xsl:with-param name="elementDesc" select="'reference to reference flow in section quantitative reference'"/>
            <xsl:with-param name="messagePrefix"/>
        </xsl:call-template>

        <xsl:call-template name="checkPresence">
            <xsl:with-param name="element" select="process:processInformation/process:time/common:referenceYear"/>
            <xsl:with-param name="elementDesc" select="'reference year'"/>
            <xsl:with-param name="messagePrefix"/>
        </xsl:call-template>

        <xsl:call-template name="checkPresence">
            <xsl:with-param name="element" select="process:processInformation/process:time/common:dataSetValidUntil"/>
            <xsl:with-param name="elementDesc" select="'data set valid until'"/>
            <xsl:with-param name="messagePrefix"/>
        </xsl:call-template>

        <xsl:call-template name="checkPresence">
            <xsl:with-param name="element" select="process:processInformation/process:geography/process:locationOfOperationSupplyOrProduction/@location"/>
            <xsl:with-param name="elementDesc" select="'location'"/>
            <xsl:with-param name="messagePrefix"/>
        </xsl:call-template>

        <xsl:call-template name="checkPresence">
            <xsl:with-param name="element" select="process:processInformation/process:technology/process:technologyDescriptionAndIncludedProcesses"/>
            <xsl:with-param name="elementDesc" select="'technology description'"/>
            <xsl:with-param name="messagePrefix"/>
        </xsl:call-template>

        <xsl:call-template name="checkPresence">
            <xsl:with-param name="element" select="process:processInformation/process:technology/process:technologicalApplicability"/>
            <xsl:with-param name="elementDesc" select="'technical purpose'"/>
            <xsl:with-param name="messagePrefix"/>
        </xsl:call-template>

        <xsl:call-template name="checkValue">
            <xsl:with-param name="element" select="process:modellingAndValidation/process:LCIMethodAndAllocation/process:typeOfDataSet"/>
            <xsl:with-param name="value1" select="'EPD'"/>
            <xsl:with-param name="elementDesc" select="'type of dataset'"/>
        </xsl:call-template>

        <xsl:call-template name="checkPresence">
            <xsl:with-param name="element" select="process:modellingAndValidation/process:LCIMethodAndAllocation/common:other/epd:subType"/>
            <xsl:with-param name="elementDesc" select="'subtype of dataset'"/>
            <xsl:with-param name="messagePrefix"/>
        </xsl:call-template>

        <xsl:call-template name="checkValue">
            <xsl:with-param name="element" select="process:modellingAndValidation/process:LCIMethodAndAllocation/common:other/epd:subType"/>
            <xsl:with-param name="value1" select="'generic dataset'"/>
            <xsl:with-param name="value2" select="'representative dataset'"/>
            <xsl:with-param name="value3" select="'average dataset'"/>
            <xsl:with-param name="value4" select="'specific dataset'"/>
            <xsl:with-param name="value5" select="'template dataset'"/>
            <xsl:with-param name="elementDesc" select="'type of dataset'"/>
        </xsl:call-template>

        <xsl:call-template name="checkPresence">
            <xsl:with-param name="element"
                select="process:modellingAndValidation/process:complianceDeclarations/process:compliance/common:referenceToComplianceSystem[@refObjectId = 'b00f9ec0-7874-11e3-981f-0800200c9a66']"/>
            <xsl:with-param name="elementDesc" select="'compliance to 15804 compliance system'"/>
            <xsl:with-param name="suppressDefaultPrefix" select="'true'"/>
            <xsl:with-param name="messagePrefix"/>
        </xsl:call-template>

        <!-- TODO: exactly one default scenario per group -->
        <!--        <xsl:if test="count(process:processInformation/process:dataSetInformation/common:other/epd:scenarios/epd:scenario)>0">
            <xsl:call-template name="checkPresence">
                <xsl:with-param name="element" select="process:processInformation/process:dataSetInformation/common:other/epd:scenarios/epd:scenarinno[@epd:default='true']"/>
                <xsl:with-param name="elementDesc" select="'Exactly one default scenario per group'"/>
                <xsl:with-param name="suppressDefaultPrefix" select="'true'"/>
                <xsl:with-param name="messagePrefix"/>
            </xsl:call-template>
        </xsl:if>
-->

        <xsl:call-template name="checkPresence">
            <xsl:with-param name="element" select="process:modellingAndValidation/process:validation/process:review/common:referenceToNameOfReviewerAndInstitution"/>
            <xsl:with-param name="elementDesc" select="'Reviewer name and institution'"/>
            <xsl:with-param name="suppressDefaultPrefix" select="'true'"/>
            <xsl:with-param name="messagePrefix"/>
        </xsl:call-template>

        <xsl:call-template name="checkPresence">
            <xsl:with-param name="element"
                select="process:administrativeInformation/process:dataEntryBy/common:referenceToDataSetFormat[@refObjectId = 'a97a0155-0234-4b87-b4ce-a45da52f2a40']"/>
            <xsl:with-param name="elementDesc" select="'Declaration of ILCD format 1.1'"/>
            <xsl:with-param name="suppressDefaultPrefix" select="'true'"/>
            <xsl:with-param name="messagePrefix"/>
        </xsl:call-template>

        <xsl:call-template name="checkPresence">
            <xsl:with-param name="element"
                select="process:administrativeInformation/process:dataEntryBy/common:referenceToDataSetFormat[@refObjectId = 'cba73800-7874-11e3-981f-0800200c9a66']"/>
            <xsl:with-param name="elementDesc" select="'Declaration of EPD Data Format Extensions'"/>
            <xsl:with-param name="suppressDefaultPrefix" select="'true'"/>
            <xsl:with-param name="messagePrefix"/>
        </xsl:call-template>

        <xsl:call-template name="checkPresence">
            <xsl:with-param name="element" select="process:administrativeInformation/process:publicationAndOwnership/common:dataSetVersion"/>
            <xsl:with-param name="elementDesc" select="'dataset version'"/>
            <xsl:with-param name="messagePrefix"/>
        </xsl:call-template>

 <!--       <xsl:call-template name="checkPresence">
            <xsl:with-param name="element" select="process:administrativeInformation/process:publicationAndOwnership/common:dateOfLastRevision"/>
            <xsl:with-param name="elementDesc" select="'date of last revision'"/>
            <xsl:with-param name="messagePrefix"/>
        </xsl:call-template>
-->
        <xsl:call-template name="checkPresence">
            <xsl:with-param name="element" select="process:administrativeInformation/process:publicationAndOwnership/common:referenceToOwnershipOfDataSet"/>
            <xsl:with-param name="elementDesc" select="'owner of dataset'"/>
            <xsl:with-param name="messagePrefix"/>
        </xsl:call-template>

        <xsl:apply-templates/>

    </xsl:template>

    <xsl:template match="*[local-name()='classificationInformation']" priority="1000"/>

</xsl:stylesheet>
