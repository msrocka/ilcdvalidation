<?xml version="1.0" encoding="UTF-8"?>
<!-- ILCD Format Version 1.1_SNAPSHOT Tools Build 1016 -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:exslt="http://exslt.org/common" xmlns:categories="http://lca.jrc.it/ILCD/Categories"
    xmlns:locations="http://lca.jrc.it/ILCD/Locations" xmlns:lciamethodologies="http://lca.jrc.it/ILCD/LCIAMethodologies" xmlns:process="http://lca.jrc.it/ILCD/Process"
    xmlns:lciamethod="http://lca.jrc.it/ILCD/LCIAMethod" xmlns:flow="http://lca.jrc.it/ILCD/Flow" xmlns:flowproperty="http://lca.jrc.it/ILCD/FlowProperty"
    xmlns:unitgroup="http://lca.jrc.it/ILCD/UnitGroup" xmlns:source="http://lca.jrc.it/ILCD/Source" xmlns:contact="http://lca.jrc.it/ILCD/Contact"
    xmlns:common="http://lca.jrc.it/ILCD/Common">

    <xsl:import href="common.xsl"/>
    
     <xsl:template name="checkFieldsProcessEL">
        
         <xsl:call-template name="checkPresence2">
            <xsl:with-param name="element" select="process:processInformation/process:dataSetInformation/common:UUID"/>
            <xsl:with-param name="elementDesc" select="'UUID'"/>
        </xsl:call-template>
        
        <xsl:call-template name="checkPresence2">
            <xsl:with-param name="element" select="process:processInformation/process:dataSetInformation/process:name/process:baseName"/>
            <xsl:with-param name="elementDesc" select="'name'"/>
        </xsl:call-template>
        
        <xsl:call-template name="checkPresence2">
            <xsl:with-param name="element" select="process:processInformation/process:quantitativeReference/process:referenceToReferenceFlow"/>
            <xsl:with-param name="elementDesc" select="'reference to reference flow in section quantitative reference'"/>
        </xsl:call-template>
        
        <xsl:call-template name="checkPresence2">
            <xsl:with-param name="element" select="process:processInformation/process:time/common:referenceYear"/>
            <xsl:with-param name="elementDesc" select="'reference year'"/>
        </xsl:call-template>
        
        <xsl:call-template name="checkPresence2">
            <xsl:with-param name="element" select="process:processInformation/process:time/common:dataSetValidUntil"/>
            <xsl:with-param name="elementDesc" select="'data set valid until'"/>
        </xsl:call-template>
        
        <xsl:call-template name="checkPresence2">
            <xsl:with-param name="element" select="process:processInformation/process:geography/process:locationOfOperationSupplyOrProduction/@location"/>
            <xsl:with-param name="elementDesc" select="'location'"/>
        </xsl:call-template>
        
        <xsl:call-template name="checkPresence2">
            <xsl:with-param name="element" select="process:processInformation/process:technology/process:technologyDescriptionAndIncludedProcesses"/>
            <xsl:with-param name="elementDesc" select="'technology description'"/>
        </xsl:call-template>
        
        <xsl:call-template name="checkPresence2">
            <xsl:with-param name="element" select="process:processInformation/process:technology/process:technologicalApplicability"/>
            <xsl:with-param name="elementDesc" select="'technical purpose'"/>
        </xsl:call-template>
        
<!--        <xsl:call-template name="checkPresence2">
            <xsl:with-param name="element" select="process:modellingAndValidation/process:validation/process:review/common:referenceToNameOfReviewerAndInstitutionS"/>
            <xsl:with-param name="elementDesc" select="'Reviewer name and institution'"/>
            <xsl:with-param name="suppressDefaultPrefix" select="'true'"/>
        </xsl:call-template>
-->        
        <xsl:call-template name="checkPresence2">
            <xsl:with-param name="element" select="process:modellingAndValidation/process:validation/process:review[@type='Accredited third party review' or @type='Independent external review' or @type='Independent review panel']/common:referenceToCompleteReviewReport"/>
            <xsl:with-param name="elementDesc" select="'Review report'"/>
            <xsl:with-param name="suppressDefaultPrefix" select="'true'"/>
        </xsl:call-template>
        
        <xsl:call-template name="checkPresence2">
            <xsl:with-param name="element" select="process:administrativeInformation/process:dataEntryBy/common:referenceToDataSetFormat[@refObjectId='a97a0155-0234-4b87-b4ce-a45da52f2a40']"/>
            <xsl:with-param name="elementDesc" select="'Declaration of ILCD format 1.1'"/>
            <xsl:with-param name="suppressDefaultPrefix" select="'true'"/>
        </xsl:call-template>
        
        <xsl:call-template name="checkPresence2">
            <xsl:with-param name="element" select="process:administrativeInformation/process:publicationAndOwnership/common:dataSetVersion"/>
            <xsl:with-param name="elementDesc" select="'dataset version'"/>
        </xsl:call-template>
        
        <xsl:call-template name="checkPresence2">
            <xsl:with-param name="element" select="process:administrativeInformation/process:publicationAndOwnership/common:dateOfLastRevision"/>
            <xsl:with-param name="elementDesc" select="'date of last revision'"/>
        </xsl:call-template>
        
        <xsl:call-template name="checkPresence2">
            <xsl:with-param name="element" select="process:administrativeInformation/process:publicationAndOwnership/common:referenceToOwnershipOfDataSet"/>
            <xsl:with-param name="elementDesc" select="'owner of dataset'"/>
        </xsl:call-template>
              
    </xsl:template>
    

    
</xsl:stylesheet>
