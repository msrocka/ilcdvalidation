<?xml version="1.0" encoding="UTF-8"?>
<!-- ILCD Format Version 1.1_SNAPSHOT Tools Build 1016 -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:exslt="http://exslt.org/common" xmlns:categories="http://lca.jrc.it/ILCD/Categories"
    xmlns:locations="http://lca.jrc.it/ILCD/Locations" xmlns:lciamethodologies="http://lca.jrc.it/ILCD/LCIAMethodologies" xmlns:process="http://lca.jrc.it/ILCD/Process"
    xmlns:lciamethod="http://lca.jrc.it/ILCD/LCIAMethod" xmlns:flow="http://lca.jrc.it/ILCD/Flow" xmlns:flowproperty="http://lca.jrc.it/ILCD/FlowProperty"
    xmlns:unitgroup="http://lca.jrc.it/ILCD/UnitGroup" xmlns:source="http://lca.jrc.it/ILCD/Source" xmlns:contact="http://lca.jrc.it/ILCD/Contact"
    xmlns:common="http://lca.jrc.it/ILCD/Common">

    <xsl:import href="validate.xsl"/>

    <xsl:output indent="no" method="text"/>

    <xsl:variable name="version" select="'1.1'"/>

    <xsl:param name="disableCheckForeignCategories" select="false()"/>

    <xsl:param name="noEmptyClassification" select="false()"/>

    <xsl:param name="pathPrefix">
        <xsl:choose>
            <xsl:when test="local-name(/*)=$WRAPPER">
                <xsl:value-of select="'./'"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="'../'"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:param>

 
    <xsl:template match="/">
        <xsl:apply-templates select="/*/@version"/>
        <xsl:apply-templates/>
    </xsl:template>


    <xsl:template match="ILCD">
        <xsl:for-each select="child::*">
            <xsl:apply-templates/>
        </xsl:for-each>
    </xsl:template>


    <!-- process dataset-specific validation -->
    <xsl:template match="process:processDataSet|flow:flowDataSet|flowproperty:flowPropertyDataSet|unitgroup:unitGroupDataSet|lciamethod:LCIAMethodDataSet|source:sourceDataSet|contact:contactDataSet">
        <xsl:apply-templates select="*/*[local-name()='classificationInformation']"/>
    </xsl:template>


    <xsl:template match="*[local-name()='classificationInformation']">
        <xsl:if test="$noEmptyClassification='true'">
            <xsl:if test="count(child::*)=0">
                <xsl:call-template name="warn">
                    <xsl:with-param name="message">classificationInformation is empty (checking with noEmptyClassification switch explicitly set to true).</xsl:with-param>
                </xsl:call-template>
            </xsl:if>
        </xsl:if>
        <xsl:apply-templates/>
    </xsl:template>


    <!-- check category information -->
    <xsl:template match="common:classification|common:elementaryFlowCategorization">
        <xsl:variable name="name">
            <xsl:choose>
                <xsl:when test="@name">
                    <xsl:value-of select="@name"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="'ILCD'"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <!-- check foreign categories only if $disableCheckForeignCategories is not true -->
        <xsl:if test="$name='ILCD' or not($disableCheckForeignCategories='true')">
            <!-- prepare categories file -->
            <xsl:variable name="categoriesFile">
                <xsl:choose>
                    <xsl:when test="@classes">
                        <xsl:value-of select="@classes"/>
                    </xsl:when>
                    <xsl:when test="@categories">
                        <xsl:value-of select="@categories"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:choose>
                            <xsl:when test="local-name()='classification'">
                                <xsl:choose>
                                    <xsl:when test="document(concat($pathPrefix, $defaultCategoriesFile), /)/categories:CategorySystem">
                                        <xsl:value-of select="concat($pathPrefix, $defaultCategoriesFile)"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="$referenceCategoriesFile"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:choose>
                                    <xsl:when test="document(concat($pathPrefix, $defaultFlowCategoriesFile), /)/categories:CategorySystem">
                                        <xsl:value-of select="concat($pathPrefix, $defaultFlowCategoriesFile)"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="$referenceFlowCategoriesFile"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:variable name="validCategories">
                <xsl:choose>
                    <xsl:when test="@classes">
                        <xsl:copy-of select="document(@classes, /)"/>
                    </xsl:when>
                    <xsl:when test="@categories">
                        <xsl:copy-of select="document(@categories, /)"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:choose>
                            <xsl:when test="local-name()='classification'">
                                <xsl:choose>
                                    <xsl:when test="document(concat($pathPrefix, $defaultCategoriesFile), /)/categories:CategorySystem">
                                        <xsl:copy-of select="document(concat($pathPrefix, $defaultCategoriesFile), /)"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:copy-of select="document($referenceCategoriesFile)"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:choose>
                                    <xsl:when test="document(concat($pathPrefix, $defaultFlowCategoriesFile), /)/categories:CategorySystem">
                                        <xsl:copy-of select="document(concat($pathPrefix, $defaultFlowCategoriesFile), /)"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:copy-of select="document($referenceFlowCategoriesFile)"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>

            <!-- check if categories file is available -->
            <xsl:choose>
                <xsl:when test="exslt:node-set($validCategories)/categories:CategorySystem">
                    <xsl:variable name="dataSetShortName">
                        <xsl:call-template name="determineDataSetShortName"/>
                    </xsl:variable>
                    <!-- checking categories-->
                    <xsl:call-template name="check_hierarchy">
                        <xsl:with-param name="messagePrefix">Validation error for <xsl:value-of select="local-name()"/> "<xsl:value-of select="$name"/>", using file "<xsl:value-of
                                select="$categoriesFile"/>"</xsl:with-param>
                        <xsl:with-param name="tree" select="exslt:node-set($validCategories)/categories:CategorySystem/categories:categories[@dataType=$dataSetShortName]"/>
                    </xsl:call-template>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:call-template name="validationEvent">
                        <xsl:with-param name="fancyMessages" select="$fancyMessages"/>
                        <xsl:with-param name="message">Could not open classification file "<xsl:value-of select="$categoriesFile"/>" for checking classification "<xsl:value-of
                                select="$name"/>"</xsl:with-param>
                    </xsl:call-template>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>
    </xsl:template>

 
    <xsl:template match="*|@*">
        <xsl:apply-templates select="*|@*"/>
    </xsl:template>

    <xsl:template match="text()"/>

    <xsl:template match="/*/@version"/>


</xsl:stylesheet>
