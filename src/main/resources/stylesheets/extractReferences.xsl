<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:flow="http://lca.jrc.it/ILCD/Flow" xmlns:process="http://lca.jrc.it/ILCD/Process"
    xmlns:common="http://lca.jrc.it/ILCD/Common" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="1.0">

    <xsl:param name="skipReferencesToLCIAMethods" select="'false'"/>
    <xsl:param name="skipComplementingProcess" select="'false'"/>

    <xsl:template match="/">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="/*">
        <xsl:element name="dataset">
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="*[local-name() = 'referenceToLCIAMethodDataSet' or local-name() = 'referenceToSupportedImpactAssessmentMethods']">
        <xsl:if test="string($skipReferencesToLCIAMethods) = 'false'">
            <xsl:element name="reference">
                <xsl:attribute name="uri">
                    <xsl:value-of select="@uri"/>
                </xsl:attribute>
                <xsl:attribute name="refObjectId">
                    <xsl:value-of select="@refObjectId"/>
                </xsl:attribute>
                <xsl:attribute name="type">
                    <xsl:value-of select="@type"/>
                </xsl:attribute>
            </xsl:element>
        </xsl:if>
    </xsl:template>

    <xsl:template match="*[local-name() = 'referenceToComplementingProcess']">
        <xsl:if test="string($skipComplementingProcess) = 'false'">
            <xsl:element name="reference">
                <xsl:attribute name="uri">
                    <xsl:value-of select="@uri"/>
                </xsl:attribute>
                <xsl:attribute name="refObjectId">
                    <xsl:value-of select="@refObjectId"/>
                </xsl:attribute>
                <xsl:attribute name="type">
                    <xsl:value-of select="@type"/>
                </xsl:attribute>
            </xsl:element>
        </xsl:if>
    </xsl:template>

    <xsl:template
        match="
            *[local-name() = 'referenceToCommissioner' or
            local-name() = 'referenceToCompleteReviewReport' or
            local-name() = 'referenceToComplianceSystem' or
            local-name() = 'referenceToContact' or
            local-name() = 'referenceToConvertedOriginalDataSetFrom' or
            local-name() = 'referenceToDataHandlingPrinciples' or
            local-name() = 'referenceToDataSetFormat' or
            local-name() = 'referenceToDataSetUseApproval' or
            local-name() = 'referenceToDataSource' or
            local-name() = 'referenceToEntitiesWithExclusiveAccess' or
            local-name() = 'referenceToExternalDocumentation' or
            local-name() = 'referenceToFlowDataSet' or
            local-name() = 'referenceToFlowPropertyDataSet' or
            local-name() = 'referenceToIncludedProcesses' or
            local-name() = 'referenceToIncludedSubMethods' or
            local-name() = 'referenceToLCAMethodDetails' or
            local-name() = 'referenceToLCIAMethodFlowDiagrammOrPicture' or
            local-name() = 'referenceToLogo' or
            local-name() = 'referenceToNameOfReviewerAndInstitution' or
            local-name() = 'referenceToOwnershipOfDataSet' or
            local-name() = 'referenceToPersonOrEntityEnteringTheData' or
            local-name() = 'referenceToPersonOrEntityGeneratingTheDataSet' or
            local-name() = 'referenceToPrecedingDataSetVersion' or
            local-name() = 'referenceToRawDataDocumentation' or
            local-name() = 'referenceToReferenceUnitGroup' or
            local-name() = 'referenceToRegistrationAuthority' or
            local-name() = 'referenceToSource' or
            local-name() = 'referenceToTechnicalSpecification' or
            local-name() = 'referenceToTechnologyFlowDiagrammOrPicture' or
            local-name() = 'referenceToTechnologyPictogramme' or
            local-name() = 'referenceToUnchangedRepublication']">
        <xsl:element name="reference">
            <xsl:attribute name="uri">
                <xsl:value-of select="@uri"/>
            </xsl:attribute>
            <xsl:attribute name="refObjectId">
                <xsl:value-of select="@refObjectId"/>
            </xsl:attribute>
            <xsl:attribute name="type">
                <xsl:value-of select="@type"/>
            </xsl:attribute>
        </xsl:element>
    </xsl:template>

    <xsl:template match="*[local-name() = 'referenceToDigitalFile']">
        <xsl:element name="reference">
            <xsl:attribute name="uri">
                <xsl:value-of select="@uri"/>
            </xsl:attribute>
            <xsl:attribute name="refObjectId">
                <xsl:value-of select="@refObjectId"/>
            </xsl:attribute>
            <xsl:attribute name="type">other external file</xsl:attribute>
        </xsl:element>
    </xsl:template>

    <xsl:template match="*">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="text()"/>

</xsl:stylesheet>
