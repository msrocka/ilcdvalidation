package com.okworx.ilcd.validation.test.util;

import static org.junit.Assert.assertEquals;

import java.io.File;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import com.okworx.ilcd.validation.ReferenceFlowValidator;
import com.okworx.ilcd.validation.reference.ReferenceCache;

public class ReferenceCacheTest {

	protected final Logger log = org.apache.log4j.Logger.getLogger(this.getClass());

	private ReferenceCache cache = new ReferenceCache();

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testFromDir() {
		this.cache.initializeFromDir(new File("src/test/resources/datasets/ReferenceFlows/reference"));
		assertEquals(237, this.cache.getLinks().size());
	}

	@Test
	public void testFromZIP() {
		this.cache.initializeFromArchive(new File("src/test/resources/datasets/ReferenceFlows/reference.zip"));
		assertEquals(237, this.cache.getLinks().size());
	}

}
