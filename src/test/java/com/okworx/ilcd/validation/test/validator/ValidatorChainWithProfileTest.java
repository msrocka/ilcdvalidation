package com.okworx.ilcd.validation.test.validator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.net.URL;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.okworx.ilcd.validation.IDatasetsValidator;
import com.okworx.ilcd.validation.SchemaValidator;
import com.okworx.ilcd.validation.ValidatorChain;
import com.okworx.ilcd.validation.XSLTStylesheetValidator;
import com.okworx.ilcd.validation.events.IValidationEvent;
import com.okworx.ilcd.validation.profile.ProfileManager;



// TODO add more test vectors 
public class ValidatorChainWithProfileTest extends AbstractValidatorTest {

	protected final Logger log = org.apache.log4j.Logger.getLogger(this.getClass());

	protected ValidatorChain validator;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		this.validator = new ValidatorChain("Composite Validator");
		
		IDatasetsValidator v1 = new SchemaValidator();

		this.validator.setProfile(ProfileManager.INSTANCE.registerProfile(new URL("file:src/main/profiles/EPD_1.1_profile_1.0.7.jar")));
			
		IDatasetsValidator v2 = new XSLTStylesheetValidator();

		this.validator.add(v1);
		this.validator.add(v2);
	}

	@After
	public void tearDown() throws Exception {
	}

	private boolean runValidation(File file) {
		
		validator.reset();
		
		log.info("setting file");
		
		validator.setObjectsToValidate(file);
		
		log.info("validating");
		
		boolean result = validator.validate();

		for (IValidationEvent event : validator.getEventsList().getEvents()) {
			log.info(event);
		}

		return result;
	}

	@Test
	public void testPass() {

		boolean result = runValidation(new File("src/test/resources/datasets/EPD/ILCD/processes/Kalk_(CaO__Feinkalk).xml"));

		assertTrue(result);
		assertTrue(validator.getEventsList().isEmpty());
		
		assertEquals(1, validator.getStatistics().getTotalValidCount());
		assertEquals(0, validator.getStatistics().getTotalInvalidCount());


	}

	@Test
	public void testFail() {

		boolean result = runValidation(new File("src/test/resources/datasets/EPD/fail"));

		assertFalse(result);
		assertEquals(6, validator.getEventsList().size());

		assertEquals(1, validator.getStatistics().getInvalidProcessesCount());
		assertEquals(0, validator.getStatistics().getInvalidContactsCount());
		
		assertEquals(0, validator.getStatistics().getTotalValidCount());
		assertEquals(1, validator.getStatistics().getTotalInvalidCount());
		
	}

}
