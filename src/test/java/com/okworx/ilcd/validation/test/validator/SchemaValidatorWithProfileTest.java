package com.okworx.ilcd.validation.test.validator;

import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

import com.okworx.ilcd.validation.SchemaValidator;
import com.okworx.ilcd.validation.profile.ProfileManager;
import com.okworx.ilcd.validation.test.util.SimpleUpdateEventListener;

// TODO add more test vectors 
public class SchemaValidatorWithProfileTest extends SchemaValidatorTest {

	protected final Logger log = org.apache.log4j.Logger.getLogger(this.getClass());

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	@Override
	public void setUp() throws Exception {
		this.validator = new SchemaValidator();

		this.validator.setValidateArchives(true);

		this.validator.setUpdateEventListener(new SimpleUpdateEventListener());

		this.validator.setProfile(ProfileManager.INSTANCE.getDefaultProfile());
	}

}
