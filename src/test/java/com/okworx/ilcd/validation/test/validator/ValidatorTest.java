package com.okworx.ilcd.validation.test.validator;



import java.io.File;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.okworx.ilcd.validation.ILCDFormatValidator;
import com.okworx.ilcd.validation.Validator;



public class ValidatorTest {

	protected final Logger log = org.apache.log4j.Logger.getLogger( this.getClass() );

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testPass() throws InterruptedException {

		Validator.validate( new File( "src/test/resources/datasets/Schema/test_pass" ), new ILCDFormatValidator() );

	}

	@Test
	public void testPassZIP() throws InterruptedException {

		Validator.validate( new File( "src/test/resources/datasets/Schema/test_pass.zip" ), new ILCDFormatValidator() );

	}

	@Test
	public void testFail() {
		// validator.setObjectsToValidate(new File("src/test/resources/datasets/Schema/test_fail"));
	}

}
