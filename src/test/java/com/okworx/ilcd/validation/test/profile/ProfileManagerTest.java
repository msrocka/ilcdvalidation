package com.okworx.ilcd.validation.test.profile;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.net.URL;

import org.apache.log4j.Logger;
import org.junit.Test;

import com.okworx.ilcd.validation.exception.InvalidProfileException;
import com.okworx.ilcd.validation.profile.ProfileManager;

public class ProfileManagerTest {

	protected final Logger log = org.apache.log4j.Logger.getLogger(this.getClass());

	@Test
	public void testLoadProfile() throws IOException {

		ProfileManager pm = ProfileManager.INSTANCE;
		log.info(pm.getDefaultProfile().getResourceAsStream("eu/europa/ec/jrc/lca/ilcd/schemas/ext/xml.xsd")
				.available());

	}

	@Test
	public void testLoadDefaultProfile() throws IOException {

		ProfileManager pm = ProfileManager.INSTANCE;
	}

	@Test
	public void testLoadEPDProfile() throws IOException, InvalidProfileException {

		ProfileManager pm = ProfileManager.INSTANCE;
		pm.registerProfile(new URL("file:src/main/profiles/EPD_1.1_profile_1.0.7.jar"));
//		System.out.println(pm.getCurrentProfile().getName());
//		System.out.println(pm.getCurrentProfile().getResourceAsStream("edu/kit/iai/lca/epd/schemas/EPD_DataSet.xsd")
//				.available());

	}

	@Test
	public void testExtractCommaSeparatedValues() throws IOException {

		ProfileManager pm = ProfileManager.INSTANCE;
		String line1 = "foo,bar,foobar";
		String[] result1 = pm.extractCommaSeparatedValues(line1);
		assertEquals(new String[] { "foo", "bar", "foobar" }, result1);
	}

}
