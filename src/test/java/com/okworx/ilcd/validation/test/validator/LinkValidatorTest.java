package com.okworx.ilcd.validation.test.validator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.net.MalformedURLException;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.okworx.ilcd.validation.AbstractDatasetsValidator;
import com.okworx.ilcd.validation.LinkValidator;
import com.okworx.ilcd.validation.exception.InvalidProfileException;
import com.okworx.ilcd.validation.profile.Profile;
import com.okworx.ilcd.validation.profile.ProfileManager;
import com.okworx.ilcd.validation.test.util.SimpleUpdateEventListener;

// TODO add more test vectors 
public class LinkValidatorTest extends AbstractValidatorTest {

	protected final Logger log = org.apache.log4j.Logger.getLogger(this.getClass());

	protected LinkValidator validator;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		this.validator = new LinkValidator();
	}

	@After
	public void tearDown() throws Exception {
	}

	private boolean runValidation(AbstractDatasetsValidator validator, File file) {
		validator.setObjectsToValidate(file);

		this.validator.setUpdateEventListener(new SimpleUpdateEventListener());

		boolean result = false;

		try {
			result = validator.validate();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// logEvents(validator);

		return result;
	}

	@Test
	public void testPass() {

		boolean result = runValidation(validator, new File("src/test/resources/datasets/Links/test_pass"));

		logEvents(validator);

		assertTrue(result);
		assertTrue(validator.getEventsList().isEmpty());

		log.info(validator.getStatistics());

		assertEquals(7, validator.getStatistics().getValidContactsCount());
		assertEquals(1, validator.getStatistics().getValidFlowPropertiesCount());
		assertEquals(1, validator.getStatistics().getValidLCIAMethodsCount());
		assertEquals(5, validator.getStatistics().getValidFlowsCount());
		assertEquals(13, validator.getStatistics().getValidSourcesCount());
		assertEquals(2, validator.getStatistics().getValidProcessesCount());
		assertEquals(1, validator.getStatistics().getValidUnitGroupsCount());

		assertEquals(0, validator.getStatistics().getTotalInvalidCount());
	}

	@Test
	public void testFail() {

		boolean result = runValidation(validator, new File("src/test/resources/datasets/Links/test_fail"));

		logEvents(validator);

		assertFalse(result);
		assertEquals(15, validator.getEventsList().size());

		log.info(validator.getStatistics());
		
		assertEquals(4, validator.getStatistics().getValidContactsCount());
		assertEquals(1, validator.getStatistics().getValidFlowPropertiesCount());
		assertEquals(3, validator.getStatistics().getValidFlowsCount());
		assertEquals(8, validator.getStatistics().getValidSourcesCount());
		assertEquals(0, validator.getStatistics().getValidProcessesCount());
		assertEquals(1, validator.getStatistics().getValidUnitGroupsCount());

		assertEquals(1, validator.getStatistics().getInvalidContactsCount());
		assertEquals(0, validator.getStatistics().getInvalidFlowPropertiesCount());
		assertEquals(0, validator.getStatistics().getInvalidFlowsCount());
		assertEquals(3, validator.getStatistics().getInvalidSourcesCount());
		assertEquals(1, validator.getStatistics().getInvalidProcessesCount());
		assertEquals(0, validator.getStatistics().getInvalidUnitGroupsCount());

		assertEquals(5, validator.getStatistics().getTotalInvalidCount());

	}

	@Test
	public void testSuccessSkipReferenceObjects() {

		validator.reset();
		validator.setParameter(LinkValidator.PARAM_SKIP_REFERENCE_OBJECTS, true);

		try {
			Profile p = ProfileManager.INSTANCE.registerProfile(new File("src/main/profiles/EF_profile_1.0.9.jar").toURI().toURL());
			validator.setProfile(p);
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (InvalidProfileException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		
		boolean result = runValidation(validator, new File("src/test/resources/datasets/Links/test_skiprefobjects"));
		logEvents(validator);


		log.info(validator.getStatistics());
		log.info( validator.getEventsList().size() + " events");
		
		assertTrue(result);
		assertEquals(0, validator.getEventsList().size());
		
		assertEquals(2, validator.getStatistics().getValidProcessesCount());
		assertEquals(0, validator.getStatistics().getInvalidProcessesCount());

		assertEquals(0, validator.getStatistics().getTotalInvalidCount());

	}

	@Test
	public void testFailSkipReferenceObjects() {

		validator.reset();
		
		boolean result = runValidation(validator, new File("src/test/resources/datasets/Links/test_skiprefobjects"));

		logEvents(validator);

		assertFalse(result);
		assertEquals(11, validator.getEventsList().size());

		log.info(validator.getStatistics());
		
		assertEquals(0, validator.getStatistics().getValidProcessesCount());
		assertEquals(2, validator.getStatistics().getInvalidProcessesCount());

		assertEquals(3, validator.getStatistics().getTotalInvalidCount());

	}

	
	@Test
	public void testSuccessSkipCP() {

		validator.reset();
		validator.setParameter(LinkValidator.PARAM_SKIP_COMPLEMENTINGPROCESS, true);

		boolean result = runValidation(validator, new File("src/test/resources/datasets/Links/test_fail"));

		logEvents(validator);

		assertFalse(result);
		assertEquals(14, validator.getEventsList().size());

		log.info(validator.getStatistics());
		
		assertEquals(0, validator.getStatistics().getValidProcessesCount());
		assertEquals(1, validator.getStatistics().getInvalidProcessesCount());

		assertEquals(5, validator.getStatistics().getTotalInvalidCount());

	}

	@Test
	public void testSuccessSkipSIAM() {

		validator.reset();
		validator.setParameter(LinkValidator.PARAM_SKIP_REFS_TO_LCIAMETHODS, true);

		boolean result = runValidation(validator, new File("src/test/resources/datasets/Links/test_fail"));

		logEvents(validator);

		assertFalse(result);
		assertEquals(13, validator.getEventsList().size());

		log.info(validator.getStatistics());
		
		assertEquals(0, validator.getStatistics().getValidProcessesCount());
		assertEquals(1, validator.getStatistics().getInvalidProcessesCount());

		assertEquals(5, validator.getStatistics().getTotalInvalidCount());

	}

	@Test
	public void testSuccessSkipCPAndSIAM() {

		validator.reset();
		validator.setParameter(LinkValidator.PARAM_SKIP_COMPLEMENTINGPROCESS, true);
		validator.setParameter(LinkValidator.PARAM_SKIP_REFS_TO_LCIAMETHODS, true);

		boolean result = runValidation(validator, new File("src/test/resources/datasets/Links/test_fail"));

		logEvents(validator);

		assertFalse(result);
		assertEquals(12, validator.getEventsList().size());

		log.info(validator.getStatistics());
		
		assertEquals(0, validator.getStatistics().getValidProcessesCount());
		assertEquals(1, validator.getStatistics().getInvalidProcessesCount());

		assertEquals(5, validator.getStatistics().getTotalInvalidCount());

	}


	
	@Test
	public void testPassZIP() {

		boolean result = runValidation(validator, new File("src/test/resources/datasets/Links/test_pass.zip"));

		logEvents(validator);

		assertTrue(result);
		assertTrue(validator.getEventsList().isEmpty());

	}

	@Test
	public void testFailZIP() {

		boolean result = runValidation(validator, new File("src/test/resources/datasets/Links/test_fail.zip"));

		logEvents(validator);

		assertFalse(result);
		assertEquals(12, validator.getEventsList().size());

	}

	@Test
	public void testUri() {

		boolean result = runValidation(validator, new File("src/test/resources/datasets/Links/test_uri"));

		logEvents(validator);

		assertFalse(result);
		assertEquals(5, validator.getEventsList().size());

	}

}
