package com.okworx.ilcd.validation.test.profile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import org.apache.log4j.Logger;
import org.junit.Test;

import com.okworx.ilcd.validation.profile.ProfileManager;
import com.okworx.ilcd.validation.util.StandaloneLocator;

public class ProfileManagerParamsTest {

	protected final Logger log = org.apache.log4j.Logger.getLogger(this.getClass());

	@Test
	public void testLoadDefaultProfile() throws IOException {
		File tempDir = Files.createTempDirectory("ilcd-test-profiles").toFile();
		new ProfileManager.ProfileManagerBuilder()
			.cacheDir(tempDir)
			.locator(new StandaloneLocator())
			.registerDefaultProfiles(true)
			.build();
		log.info(ProfileManager.getInstance().getCacheDir().getAbsolutePath());
	}

}
